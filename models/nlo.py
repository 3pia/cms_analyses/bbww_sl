# coding: utf-8

from utils.processes import Process, ProcessesMixin, classproperty
from .common import StatModelBase
import warnings
import re


# Add processes
class BackgroundProcesses(ProcessesMixin):
    """
    Background Processes
    They have to be > 0
    """

    @classproperty
    def _processes(cls):
        return {
            # Process(name="GluGluHToTauTau_M125", id=1),
            # Process(name="GluGluHToBB_M125", id=2),
            Process(name="dc_ggHbb", id=1),
            Process(name="GluGluHToWWToLNuQQ_M125_NNPDF31", id=2),
            # Process(name="VBFHToTauTau_M125", id=4),
            # Process(name="VBFHToBB_M_125", id=5),
            Process(name="dc_qqHbb", id=3),
            Process(name="VBFHToWWToLNuQQ_M125_NNPDF31", id=4),
            Process(name="WH", id=5),
            Process(name="dc_ZHbb", id=6),
            # Process(name="ZHToTauTau_M125", id=9),
            Process(name="ttH", id=7),
            Process(name="ttVH", id=8),
            Process(name="THQ_ctcvcp_4f_Hincl", id=9),
            Process(name="THW_ctcvcp_5f_Hincl", id=10),
            Process(name="wjets", id=11),
            Process(name="dy", id=12),
            Process(name="st", id=13),
            Process(name="tt", id=14),
            Process(name="vv", id=15),
            Process(name="vvv", id=16),
            Process(name="ttZ", id=17),
            Process(name="ttW", id=18),
            Process(name="ttVV", id=19),
            Process(name="dc_others", id=20),
            Process(name="fakes", id=21),
        }


class NLOProcesses(BackgroundProcesses):
    """
    Signal Processes
    # They have to be <= 0
    """

    @classproperty
    def _processes(cls):
        _processes = super()._processes
        return {
            # bbtt
            Process(name="qqHH_CV_1_C2V_1_kl_0_2B2Tau", id=-21),
            Process(name="qqHH_CV_1_C2V_1_kl_1_2B2Tau", id=-20),
            Process(name="qqHH_CV_1_C2V_1_kl_2_2B2Tau", id=-19),
            Process(name="qqHH_CV_1_C2V_2_kl_1_2B2Tau", id=-18),
            Process(name="qqHH_CV_1_5_C2V_1_kl_1_2B2Tau", id=-17),
            Process(name="qqHH_CV_0_5_C2V_1_kl_1_2B2Tau", id=-16),
            Process(name="qqHH_CV_1_C2V_0_kl_1_2B2Tau", id=-15),
            Process(name="ggHH_kl_0_kt_1_2B2Tau", id=-14),
            Process(name="ggHH_kl_1_kt_1_2B2Tau", id=-13),
            Process(name="ggHH_kl_2p45_kt_1_2B2Tau", id=-12),
            Process(name="ggHH_kl_5_kt_1_2B2Tau", id=-11),
            # bbvv
            Process(name="qqHH_CV_1_C2V_1_kl_0_2B2V", id=-10),
            Process(name="qqHH_CV_1_C2V_1_kl_1_2B2V", id=-9),
            Process(name="qqHH_CV_1_C2V_1_kl_2_2B2V", id=-8),
            Process(name="qqHH_CV_1_C2V_2_kl_1_2B2V", id=-7),
            Process(name="qqHH_CV_1_5_C2V_1_kl_1_2B2V", id=-6),
            Process(name="qqHH_CV_0_5_C2V_1_kl_1_2B2V", id=-5),
            Process(name="qqHH_CV_1_C2V_0_kl_1_2B2V", id=-4),
            Process(name="ggHH_kl_0_kt_1_2B2V", id=-3),
            Process(name="ggHH_kl_1_kt_1_2B2V", id=-2),
            Process(name="ggHH_kl_2p45_kt_1_2B2V", id=-1),
            Process(name="ggHH_kl_5_kt_1_2B2V", id=0),
        } | _processes


class AddDipoleRecoilNuisance:
    # year, process, couplings, category, value
    # values from: https://gitlab.cern.ch/cms-hh-bbww/cms-hh-to-bbww/-/blob/master/Legacy/DipoleRecoil.md
    dipole_recoil = {
        2017: {
            "2B2Tau": {
                ("1", "1", "1"): {"boosted": 1.00, "resolved_1b": 1.17, "resolved_2b": 1.06},
                ("1", "2", "1"): {"boosted": 1.07, "resolved_1b": 1.05, "resolved_2b": 1.05},
            },
            "2B2V": {
                ("1", "1", "1"): {"boosted": 1.06, "resolved_1b": 1.07, "resolved_2b": 1.02},
                ("1", "2", "1"): {"boosted": 1.02, "resolved_1b": 1.02, "resolved_2b": 1.03},
            },
        },
        2018: {
            "2B2Tau": {
                ("1", "1", "1"): {"boosted": 1.00, "resolved_1b": 1.14, "resolved_2b": 1.08},
                ("1", "2", "1"): {"boosted": 1.08, "resolved_1b": 1.11, "resolved_2b": 1.05},
            },
            "2B2V": {
                ("1", "1", "1"): {"boosted": 1.00, "resolved_1b": 1.00, "resolved_2b": 1.00},
                ("1", "2", "1"): {"boosted": 1.01, "resolved_1b": 1.02, "resolved_2b": 1.02},
            },
        },
    }

    def __init__(self, model) -> None:
        self.year = int(model.year)
        self.model = model
        m = re.match(r"all_(.+)_sr_prompt_dnn_node_class_HHVBF_NLO", model.category)
        self.category = m.group(1)

    @staticmethod
    def extract(process: str):
        m = re.match(r"qqHH_CV_(.+)_C2V_(.+)_kl_(.+)_(2B2Tau|2B2V)", process)
        if not m:
            warnings.warn(f"AddDipoleRecoilNuisance did not match process: {process}!")
        c = (m.group(1), m.group(2), m.group(3))
        if c != ("1", "1", "1") or c != ("1", "2", "1"):
            c = None
        # final state, coupling
        return m.group(4), c

    def __call__(self):
        for process in [p for p in self.model.processes if p.startswith("qqHH_")]:
            fs, c = self.extract(process)

            if int(self.year) == 2016:
                if c is None:
                    unc17 = max(
                        self.dipole_recoil[2017][fs][("1", "1", "1")][self.category],
                        self.dipole_recoil[2017][fs][("1", "2", "1")][self.category],
                    )
                    unc18 = max(
                        self.dipole_recoil[2018][fs][("1", "1", "1")][self.category],
                        self.dipole_recoil[2018][fs][("1", "2", "1")][self.category],
                    )
                    unc = max(unc17, unc18)
                else:
                    unc = max(
                        self.dipole_recoil[2017][fs][c][self.category],
                        self.dipole_recoil[2018][fs][c][self.category],
                    )
            else:
                if c is None:
                    unc = max(
                        self.dipole_recoil[self.year][fs][("1", "1", "1")][self.category],
                        self.dipole_recoil[self.year][fs][("1", "2", "1")][self.category],
                    )
                else:
                    unc = self.dipole_recoil[self.year][fs][c][self.category]
            if unc != 1.0:
                self.model.add_systematics(
                    names={
                        "qqHH_pythiaDipoleOn": unc,
                    },
                    type="lnN",
                    processes=[process],
                )


class NLOStatModel(StatModelBase):
    def build_systematics(self):
        super().build_systematics()
        # https://gitlab.cern.ch/hh/naming-conventions#theory-uncertainties
        self.add_systematics(
            names={
                # alpha_s now included in pdf_Higgs_ggHH
                # "alpha_s_ggHH": 1.021,
                "pdf_Higgs_ggHH": 1.030,
                # scale uncertainty and top mass unc will be added by PhysicsModel
                # "QCDscale_ggHH": (0.950, 1.022),
                # "m_top_unc_ggHH": 1.026,
            },
            type="lnN",
            processes=[p for p in self.processes if p.startswith("ggHH_")],
        )
        self.add_systematics(
            names={
                "pdf_Higgs_qqHH": 1.021,
                "QCDscale_qqHH": (0.9996, 1.0003),
            },
            type="lnN",
            processes=[p for p in self.processes if p.startswith("qqHH_")],
        )
        if "HHVBF" in self.category:
            self.add_systematics(
                names={"qqHH_pythiaDipoleOn": 1.1},
                type="lnN",
                processes=[p for p in self.processes if p.startswith("qqHH_")],
            )


class StatModel(NLOProcesses, NLOStatModel):
    pass


class StatModelNoSysts(NLOProcesses, NLOStatModel):
    @property
    def custom_lines(self):
        return [f"{self.bin} autoMCStats 10"]

    def build_systematics(self):
        return


class StatModelStatOnly(NLOProcesses, NLOStatModel):
    @property
    def custom_lines(self):
        return []

    def build_systematics(self):
        return


class StatModelNoMCStats(NLOProcesses, NLOStatModel):
    @property
    def custom_lines(self):
        cl = super().custom_lines
        return [ele for ele in cl if "autoMCStats" not in ele]


class Issue460GlobalLnNBase(NLOProcesses, NLOStatModel):
    """See Andrew Gilberts proposal: https://cms-talk.web.cern.ch/t/postfit-uncertainty-bands-very-large/20967/13"""

    global_lnN_strength = None

    @property
    def custom_lines(self):
        return []

    def build_systematics(self):
        assert isinstance(self.global_lnN_strength, float)
        self.add_systematics(
            names={"global_norm": self.global_lnN_strength},
            type="lnN",
            processes=set(self.processes),
        )


class Issue460GlobalLnN1p001(Issue460GlobalLnNBase):
    global_lnN_strength = 1.001


class Issue460GlobalLnN1p01(Issue460GlobalLnNBase):
    global_lnN_strength = 1.01


class Issue460GlobalLnN1p1(Issue460GlobalLnNBase):
    global_lnN_strength = 1.1


class Issue460GlobalLnN1p5(Issue460GlobalLnNBase):
    global_lnN_strength = 1.5


class Issue460ChristianSuggestion01Base(NLOProcesses, NLOStatModel):
    """Christians proposal:

    Keep only lnN for major backgrounds, and scale them by 0.1 and 0.01
    """

    lnN_scaling = None

    @property
    def custom_lines(self):
        return []

    def build_systematics(self):
        assert isinstance(self.lnN_scaling, float)
        for p, s in [("tt", 0.2), ("st", 0.2), ("wjets", 0.2)]:
            self.add_systematics(
                names={f"{p}_norm": 1.0 + s * self.lnN_scaling},
                type="lnN",
                processes=[p],
            )


class Issue460ChristianSuggestion01Scaling2p0(Issue460ChristianSuggestion01Base):
    lnN_scaling = 2.0


class Issue460ChristianSuggestion01Scaling1p0(Issue460ChristianSuggestion01Base):
    lnN_scaling = 1.0


class Issue460ChristianSuggestion01Scaling0p1(Issue460ChristianSuggestion01Base):
    lnN_scaling = 0.1


class Issue460ChristianSuggestion01Scaling0p01(Issue460ChristianSuggestion01Base):
    lnN_scaling = 0.01


class StatModelOnlyProcessNorm(NLOProcesses, NLOStatModel):
    @property
    def custom_lines(self):
        cl = super().custom_lines
        return [ele for ele in cl if "rateParam" in ele]

    def build_systematics(self):
        # fakes
        self.add_systematics(
            names={
                f"CMS_bbww_sl_fakes_norm_{self.year}": 1.5,
            },
            type="lnN",
            processes=["fakes"],
        )


class StatModelOnlyLnN(NLOProcesses, NLOStatModel):
    @property
    def custom_lines(self):
        return []

    def build_systematics(self):
        # fakes
        self.add_systematics(
            names={f"CMS_bbww_sl_fakes_norm_{self.year}": 1.5},
            type="lnN",
            processes=["fakes"],
        )

        self.add_systematics(
            names={"CMS_bbww_sl_tt_norm": 1.2},
            type="lnN",
            processes=["tt"],
        )

        self.add_systematics(
            names={"CMS_bbww_sl_st_norm": 1.2},
            type="lnN",
            processes=["st"],
        )

        self.add_systematics(
            names={"CMS_bbww_sl_wjets_norm": 1.2},
            type="lnN",
            processes=["wjets"],
        )

        if "boosted" in self.category:
            self.add_systematics(
                names={"CMS_bbww_sl_tt_norm_boost": 1.2},
                type="lnN",
                processes=["tt"],
            )

            self.add_systematics(
                names={"CMS_bbww_sl_st_norm_boost": 1.2},
                type="lnN",
                processes=["st"],
            )


class StatModelOnlyExtraLnN(NLOProcesses, NLOStatModel):
    @property
    def custom_lines(self):
        return []

    def build_systematics(self):
        # model which only contains lnN uncertainties
        # lnNs to model rateParams are set to 50%
        # can be used to plot all variables (no extra processing needed)

        # fakes
        self.add_systematics(
            names={f"CMS_bbww_sl_fakes_norm_{self.year}": 1.5},
            type="lnN",
            processes=["fakes"],
        )

        self.add_systematics(
            names={"CMS_bbww_sl_tt_norm": 1.5},
            type="lnN",
            processes=["tt"],
        )

        self.add_systematics(
            names={"CMS_bbww_sl_st_norm": 1.5},
            type="lnN",
            processes=["st"],
        )

        self.add_systematics(
            names={"CMS_bbww_sl_wjets_norm": 1.5},
            type="lnN",
            processes=["wjets"],
        )

        if "boosted" in self.category:
            self.add_systematics(
                names={"CMS_bbww_sl_tt_norm_boost": 1.2},
                type="lnN",
                processes=["tt"],
            )

            self.add_systematics(
                names={"CMS_bbww_sl_st_norm_boost": 1.2},
                type="lnN",
                processes=["st"],
            )


class StatModelExtraLnN(NLOProcesses, NLOStatModel):
    def build_systematics(self):
        super().build_systematics()
        # model uncertainties as LnN which are modeled as rateParams (used for prefit plots)
        self.add_systematics(
            names={"CMS_bbww_sl_tt_norm": 1.5},
            type="lnN",
            processes=["tt"],
        )

        self.add_systematics(
            names={"CMS_bbww_sl_st_norm": 1.5},
            type="lnN",
            processes=["st"],
        )

        self.add_systematics(
            names={"CMS_bbww_sl_wjets_norm": 1.5},
            type="lnN",
            processes=["wjets"],
        )


class EFTStatModel(BackgroundProcesses, StatModelBase):
    """
    Add SM VBF as background (recommendation by Alexandra)
    """

    _BMSignals = None

    @classproperty
    def _processes(cls):
        _processes = super()._processes
        assert cls._BMSignals is not None, "You need to set the '_BMSignals' class attribute!"
        assert isinstance(cls._BMSignals, set)
        assert all(isinstance(p, Process) for p in cls._BMSignals)
        return (
            {
                Process(name="qqHH_CV_1_C2V_1_kl_1_2B2Tau", id=22),
                Process(name="qqHH_CV_1_C2V_1_kl_1_2B2V", id=23),
            }
            | _processes
            | cls._BMSignals
        )


EFTStatModels = {}

# generate all EFT BM Statmodel classes
for BM in ["SM", "box", *[*map(str, range(1, 14))]]:
    # DL-bbVV: GluGluToHHTo2B2VTo2L2Nu_node_*
    # SL-bbWW: GluGluToHHTo2B2WToLNu2J_node_*
    # bbtt: GluGluToHHTo2B2Tau_node_*
    _BMSignals = {
        Process(f"GluGluToHHTo2B2WToLNu2J_node_{BM}", id=-2),
        Process(f"GluGluToHHTo2B2VTo2L2Nu_node_{BM}", id=-1),
        Process(f"GluGluToHHTo2B2Tau_node_{BM}", id=0),
    }
    EFTStatModels[BM] = type(
        f"BM_{BM}",
        (EFTStatModel,),
        {"_BMSignals": _BMSignals},
    )
