# coding: utf-8

import json
import os
from pathlib import Path

import hist
import numpy as np

from config.constants import LAMBDA0
from utils.binning import thresh_rebin2, wquant, smooth_systs
from utils.datacard import Datacard, RDict
from utils.util import AsteriskDict


class StatModelBase(Datacard):
    @property
    def custom_lines(self):
        lines = self.decorrelate(
            systematic="scale",
            processes=[
                self.rprocesses.get(p, p)
                for p in ["dy", "tt", "vv", "tHW", "tHq", "ttH", "ttW", "ttZ", "wjets"]
            ],
            drop=False,
        )
        lines += self.decorrelate(
            systematic="scale",
            processes={self.rprocesses[p]: "HH" for p in self.processes if p.startswith("ggHH_")},
            drop=False,
        )
        # Decorrelation according to Karl (Skype) into TT and HH:
        lines += self.decorrelate(
            systematic="ps_fsr",
            processes={
                self.rprocesses.get("tt"): self.rprocesses.get("tt"),
                **{
                    self.rprocesses[p]: "HH"
                    for p in self.processes
                    if p.startswith(("ggHH_", "qqHH_"))
                },
            },
            drop=False,
        )
        lines += self.decorrelate(
            systematic="ps_isr",
            processes={
                self.rprocesses.get("tt"): self.rprocesses.get("tt"),
                **{
                    self.rprocesses[p]: "HH"
                    for p in self.processes
                    if p.startswith(("ggHH_", "qqHH_"))
                },
            },
            drop=False,
        )
        lines += [
            "nuisance edit freeze scale ifexists",
            "nuisance edit freeze ps_fsr ifexists",
            "nuisance edit freeze ps_isr ifexists",
        ]

        rate_processes = ("tt", "st", "wjets")
        for p in rate_processes:
            if p not in self.vps:
                continue
            pname = self.rprocesses.get(p)
            rateParam_name = f"CMS_bbww_sl_{pname}_norm"
            lines += [f"{rateParam_name} rateParam * {pname} 1 [0,5]"]
        lines += [f"{self.bin} autoMCStats 10"]
        return lines

    # hook for renamings
    # rename processes
    @property
    def rprocesses(self):
        return RDict(os.path.abspath("bbww_sl/models/processes.txt"))

    def get_rnuisances(self):
        ret = RDict(os.path.abspath(f"bbww_sl/models/systematics_{self.year}.txt"))
        ret.update(
            {
                "fake_nc_nom_e": f"CMS_bbww_sl_FakeRate_nc_nom_e_{self.category}_{self.year}",
                "fake_nc_nom_mu": f"CMS_bbww_sl_FakeRate_nc_nom_mu_{self.category}_{self.year}",
                "fake_nc_slope_e": f"CMS_bbww_sl_FakeRate_nc_slope_e_{self.category}_{self.year}",
                "fake_nc_slope_mu": f"CMS_bbww_sl_FakeRate_nc_slope_mu_{self.category}_{self.year}",
                "fake_electrons:0.0-45.0_0.0-1.479": f"CMS_bbww_sl_FakeRate_e_BL_{self.year}",
                "fake_electrons:0.0-45.0_1.479-inf": f"CMS_bbww_sl_FakeRate_e_EL_{self.year}",
                "fake_electrons:45.0-65.0_0.0-1.479": f"CMS_bbww_sl_FakeRate_e_BM_{self.year}",
                "fake_electrons:45.0-65.0_1.479-inf": f"CMS_bbww_sl_FakeRate_e_EM_{self.year}",
                "fake_electrons:65.0-inf_0.0-1.479": f"CMS_bbww_sl_FakeRate_e_BH_{self.year}",
                "fake_electrons:65.0-inf_1.479-inf": f"CMS_bbww_sl_FakeRate_e_EH_{self.year}",
                "fake_muons:0.0-45.0_0.0-1.2": f"CMS_bbww_sl_FakeRate_m_BL_{self.year}",
                "fake_muons:0.0-45.0_1.2-inf": f"CMS_bbww_sl_FakeRate_m_EL_{self.year}",
                "fake_muons:45.0-65.0_0.0-1.2": f"CMS_bbww_sl_FakeRate_m_BM_{self.year}",
                "fake_muons:45.0-65.0_1.2-inf": f"CMS_bbww_sl_FakeRate_m_EM_{self.year}",
                "fake_muons:65.0-inf_0.0-1.2": f"CMS_bbww_sl_FakeRate_m_BH_{self.year}",
                "fake_muons:65.0-inf_1.2-inf": f"CMS_bbww_sl_FakeRate_m_EH_{self.year}",
            }
        )
        return ret

    # rename nuisances
    @property
    def rnuisances(self):
        return self.get_rnuisances()

    def build_systematics(self):
        # lumi https://gitlab.cern.ch/hh/naming-conventions#luminosity

        lumi = {
            2016: {
                "lumi_13TeV_2016": 1.010,
                "lumi_13TeV_correlated": 1.006,
            },
            2017: {
                "lumi_13TeV_2017": 1.020,
                "lumi_13TeV_correlated": 1.009,
                "lumi_13TeV_1718": 1.006,
            },
            2018: {
                "lumi_13TeV_2018": 1.015,
                "lumi_13TeV_correlated": 1.020,
                "lumi_13TeV_1718": 1.002,
            },
        }

        self.add_systematics(
            names=lumi[int(self.year)],
            type="lnN",
            processes=set(self.processes) - {"fakes"},
        )

        # process xsec uncertainties
        # Branching ratio lnN:
        # https://gitlab.cern.ch/hh/naming-conventions#branching-ratios
        self.add_systematics(
            names={"BR_H_BB": (0.9874, 1.0124)},
            type="lnN",
            processes=set(
                [p for p in self.processes if "2B" in p or "BB" in p] + ["WH", "dc_ZHbb", "ttH"]
            ),
        )
        self.add_systematics(
            names={"BR_H_WW": (0.9848, 1.0153)},
            type="lnN",
            processes=set([p for p in self.processes if "2W" in p or "2V" in p or "WW" in p]),
        )
        self.add_systematics(
            names={"BR_H_ZZ": (0.9848, 1.0153)},
            type="lnN",
            processes=set([p for p in self.processes if "2V" in p]),
        )
        self.add_systematics(
            names={"BR_H_TT": (0.9837, 1.0165)},
            type="lnN",
            processes=set([p for p in self.processes if "2Tau" in p or "TauTau" in p]),
        )

        # top
        # self.add_systematics(
        #     names={"CMS_bbww_sl_top_norm": 1.2},
        #     type="lnN",
        #     processes=["tt", "st"],
        # )
        # if not self.category == "all_resolved_sr_prompt_top":
        #     self.add_systematics(
        #         names={f"CMS_bbww_sl_top_norm_{self.category}_{self.year}": 1.2},
        #         type="lnN",
        #         processes=["tt", "st"],
        #     )

        # tt
        self.add_systematics(
            names={
                "m_top_unc_tt": self.reluncs("tt", uncs="mtop"),
                "pdf_gg": self.reluncs("tt", uncs="pdf"),
                "QCDscale_ttbar": self.reluncs("tt", uncs="scale"),
                # "CMS_bbww_sl_tt_norm": 1.2,
            },
            type="lnN",
            processes=["tt"],
        )
        # wjets
        self.add_systematics(
            names={
                "pdf_qqbar": self.reluncs("WJetsToLNu", uncs="pdf"),
                "QCDscale_V": self.reluncs("WJetsToLNu", uncs="scale"),
            },
            type="lnN",
            processes=["wjets"],
        )
        # self.add_systematics(
        #     names={"CMS_bbww_sl_wjets_norm": 1.2},
        #     type="lnN",
        #     processes=["wjets"],
        # )
        # if not self.category == "all_incl_sr_prompt_dnn_node_wjets":
        #     self.add_systematics(
        #         names={f"CMS_bbww_wjets_norm_{self.category}_{self.year}": 1.2},
        #         type="lnN",
        #         processes=["wjets"],
        #     )

        # fakes
        self.add_systematics(
            names={
                f"CMS_bbww_sl_fakes_norm_{self.year}": 1.5,
            },
            type="lnN",
            processes=["fakes"],
        )

        # dy
        self.add_systematics(
            names={
                "integration_dy": self.reluncs("dy_lep_50ToInf", uncs="integration"),
                "pdf_qqbar": self.reluncs("dy_lep_50ToInf", uncs="pdf"),
                "QCDscale_V": self.reluncs("dy_lep_50ToInf", uncs="scale"),
            },
            type="lnN",
            processes=["dy"],
        )
        self.add_systematics(
            names={
                "pdf_qqbar": self.reluncs("st", uncs="pdf"),
                "QCDscale_ttbar": self.reluncs("st", uncs="scale"),
                # "CMS_bbww_sl_st_norm": 1.2,
            },
            type="lnN",
            processes=["st"],
        )
        # ggH
        self.add_systematics(
            names={
                "alpha_s": 1.026,
                "pdf_Higgs_gg": 1.019,
                "QCDscale_ggH": 1.039,
            },
            type="lnN",
            processes=[
                # "GluGluHToTauTau_M125",
                # "GluGluHToBB_M125",
                "dc_ggHbb",
                "GluGluHToWWToLNuQQ_M125_NNPDF31",
            ],
        )
        # qqH
        self.add_systematics(
            names={
                "alpha_s": 1.005,
                "pdf_Higgs_qqbar": 1.021,
                "QCDscale_qqH": (0.997, 1.004),
            },
            type="lnN",
            processes=[
                # "VBFHToTauTau_M125",
                # "VBFHToBB_M_125",
                "dc_qqHbb",
                "VBFHToWWToLNuQQ_M125_NNPDF31",
            ],
        )
        # ZH
        self.add_systematics(
            names={
                "alpha_s": 1.009,
                "pdf_Higgs_qqbar": 1.013,
                "QCDscale_VH": (0.970, 1.038),
            },
            type="lnN",
            processes=[
                "dc_ZHbb",
            ],  # "ZHToTauTau_M125"],
        )
        # WH
        self.add_systematics(
            names={
                "alpha_s": 1.009,
                "pdf_Higgs_qqbar": 1.017,
                "QCDscale_VH": (0.993, 1.005),
            },
            type="lnN",
            processes=["WH"],
        )
        # ttH
        self.add_systematics(
            names={
                "alpha_s": 1.02,
                "pdf_Higgs_ttH": 1.030,
                "QCDscale_ttH": (0.908, 1.058),
            },
            type="lnN",
            processes=["ttH"],
        )
        # THQ_ctcvcp_4f_Hincl
        self.add_systematics(
            names={
                "alpha_s": 1.012,
                "pdf_Higgs_ttH": 1.035,
                "QCDscale_ttH": (0.853, 1.065),
            },
            type="lnN",
            processes=["THQ_ctcvcp_4f_Hincl"],
        )
        # THW_ctcvcp_5f_Hincl
        self.add_systematics(
            names={
                "alpha_s": 1.015,
                "pdf_Higgs_ttH": 1.061,
                "QCDscale_ttH": (0.933, 1.049),
            },
            type="lnN",
            processes=["THW_ctcvcp_5f_Hincl"],
        )
        # weighted VV uncs:
        self.add_systematics(
            names={
                "pdf_qqbar": self.reluncs("vv", uncs="pdf"),
                "QCDscale_VV": self.reluncs("vv", uncs="scale"),
            },
            type="lnN",
            processes=["vv"],
        )
        self.add_systematics(
            names={
                "pdf_gg": self.reluncs("ttZ", uncs="pdf"),
                "QCDscale_ttbar": self.reluncs("ttZ", uncs="scale"),
                "alpha_s": self.reluncs("ttZ", uncs="alpha_s"),
                "EW_corr_ttZ": (0.998, 1.0),  # https://arxiv.org/pdf/1610.07922.pdf table 40
            },
            type="lnN",
            processes=["ttZ"],
        )
        self.add_systematics(
            names={
                "pdf_qqbar": self.reluncs("ttW", uncs="pdf"),
                "QCDscale_ttbar": self.reluncs("ttW", uncs="scale"),
                "alpha_s": self.reluncs("ttW", uncs="alpha_s"),
                "EW_corr_ttW": (0.968, 1.0),  # https://arxiv.org/pdf/1610.07922.pdf table 40
            },
            type="lnN",
            processes=["ttW"],
        )
        # weighted VVV uncs:
        self.add_systematics(
            names={
                "total_VVV": self.reluncs("vvv", uncs="total"),
            },
            type="lnN",
            processes=["vvv"],
        )
        # weighted Others uncs:
        # self.add_systematics(
        #     # names={
        #     #     "pdf_Others": self.reluncs("dc_others", uncs="pdf"),
        #     #     "QCDscale_Others": self.reluncs("dc_others", uncs="scale"),
        #     # },
        #     names="total_Others",
        #     type="lnN",
        #     strength=1.5,
        #     processes=["dc_others"],
        # )

        if "boosted" in self.category:
            # self.add_systematics(
            #     names={"CMS_bbww_sl_top_norm_boost": 1.2},
            #     type="lnN",
            #     processes=["tt", "st"],
            # )
            self.add_systematics(
                names={"CMS_bbww_sl_tt_norm_boost": 1.2},
                type="lnN",
                processes=["tt"],
            )
            self.add_systematics(
                names={"CMS_bbww_sl_st_norm_boost": 1.2},
                type="lnN",
                processes=["st"],
            )

        # add shape uncs
        # misc
        self.add_systematics(
            names="top_pT_reweighting",
            type="shape",
            strength=1.0,
            processes=["tt"],
        )
        # Top dataset shifts
        from utils.topvariations import TopSystematics

        self.add_systematics(
            names=list(set(TopSystematics) - {"TopMass"}),  # TopMass left out according to Saswati
            type="shape",
            strength=1.0,
            processes={"tt", "fakes"},
        )
        self.add_systematics(
            names=[
                "PSWeight_FSR",
                "PSWeight_ISR",
            ],
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )
        # remove ST: https://hypernews.cern.ch/HyperNews/CMS/get/generators/5165.html
        self.add_systematics(
            names=[
                # "ScaleWeight_Fact",
                # "ScaleWeight_Renorm",
                # "ScaleWeight_Mixed",
                "ScaleWeight_Envelope",
            ],
            type="shape",
            strength=1.0,
            processes=set(self.processes) - {"st"},
        )
        common_shape_systs = [
            "pileup",
        ]
        if str(self.year) in ("2016", "2017"):
            common_shape_systs += ["l1_ecal_prefiring"]
        self.add_systematics(
            names=common_shape_systs,
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )
        self.add_systematics(
            names="jet_PUid_*",
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )
        self.add_systematics(
            names="btagWeight_*",
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )
        jes_uncs = self.campaign_inst.aux["jes_sources"] + ["jer", "UnclustEn"]
        if int(self.year) == 2018:
            jes_uncs.append("HemIssue")
        self.add_systematics(
            names=jes_uncs,
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )

        # lepton efficiencies
        self.add_systematics(
            names=["electron_id_loose", "electron_tth_tight", "electron_tth_relaxed"],
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )
        self.add_systematics(
            names=["muon_idiso_loose", "muon_tth_tight", "muon_tth_relaxed"],
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )
        # trigger
        self.add_systematics(
            names="trigger*",
            type="shape",
            strength=1.0,
            processes=set(self.processes),
        )

        # # Vpt_nlo
        # self.add_systematics(
        #     names="Vpt_nlo",
        #     type="shape",
        #     strength=1.0,
        #     processes=["dy", "wjets"],
        # )

        # # PDFSet
        # self.add_systematics(
        #     names="PDFSet",
        #     type="shape",
        #     strength=1.0,
        #     processes=set(self.processes),
        # )

        # Fake systematics

        # Plain
        self.add_systematics(
            names="fake_electrons:*",
            type="shape",
            strength=1.0,
            processes={"fakes"},
            collapse=True,
        )
        self.add_systematics(
            names="fake_muons:*",
            type="shape",
            strength=1.0,
            processes={"fakes"},
            collapse=True,
        )

        # Non-closure
        parameters = ["nom"]
        if "background" in self.analysis_inst.categories.get(self.category).tags:
            parameters += ["slope"]

        for par in parameters:
            self.add_systematics(
                names=f"fake_nc_{par}_*",
                type="shape",
                strength=1.0,
                processes={"fakes"},
                collapse=True,
            )

        # self.add_systematics(
        #     names="fake_nc_*",
        #     type="shape",
        #     strength=1.0,
        #     processes={"fakes"},
        #     collapse=True,
        # )

    # tt_fh: used for fake nc correction, will be erased in group_processes
    # data: implicit process for datacard
    extra_processes = ["data", "tt_fh"]

    @classmethod
    def requires(cls, task):
        from tasks.binning import Rebin

        process_group = list(cls.processes) + cls.extra_processes
        return Rebin.req(task, process_group=process_group, trim_binnings=True)

    @classmethod
    def calculate_binnings(cls, variable, h, task):
        from tqdm.auto import tqdm

        if variable.name.startswith("dnn_"):
            categories = [c for c in h.axes["category"] if "sr_" in c and "_prompt" in c]

            binnings = AsteriskDict(
                {c: None for c in task.analysis_inst.aux["binning_categories"][task.year]}
            )

            for c in tqdm(
                categories,
                unit="category",
                desc=f"calculate binning - {variable.name}",
                leave=False,
            ):
                # Calculate binning for categories which start with 'all' and are signal region and prompt (contain 'sr_prompt')
                is_valid = c.startswith("all_") and "sr_prompt" in c
                if not is_valid:
                    continue
                ctags = task.analysis_inst.categories.get(c).tags
                hview = h.view()[:, h.axes[1].index(c)].copy()
                hnom = hview[:, h.axes["systematic"].index("nominal"), :]

                bin_centers = h.axes[variable.name].centers
                bin_edges = h.axes[variable.name].edges

                """
                Processes used for rebinning:
                - backgrounds in background regions
                - signal+backgrounds in respective signal region
                """
                rebin_procs = [
                    proc
                    for proc in h.axes["process"]
                    if (proc in cls.processes) and ("HH" not in proc)
                ]
                if "signal" in ctags:
                    if "ggF" in ctags:
                        p_sig = "ggHH_kl_1_kt_1_2B2V"
                    if "VBF" in ctags:
                        p_sig = "qqHH_CV_1_C2V_1_kl_1_2B2V"
                    rebin_procs.append(p_sig)

                bidx = h.axes["process"].index(rebin_procs)

                if "signal" in ctags:
                    nb = 16j

                    sigidx = h.axes["process"].index([p_sig])
                    sigarr = np.sum(hnom[sigidx], axis=0)["value"]

                    loc = h.axes[variable.name].index

                    succesfull = False
                    while not succesfull:
                        quant = wquant(bin_centers, sigarr, np.r_[0:1:nb])
                        idx = np.digitize(quant, bin_edges) - 1

                        new_binning = np.r_[bin_edges[0], bin_edges[idx], bin_edges[-1]]
                        last_bin = hnom[bidx, loc(new_binning[-2]) : loc(new_binning[-1])].sum()

                        last_bin_content = last_bin.value
                        last_bin_relerr = np.sqrt(last_bin.variance) / last_bin_content

                        if (last_bin_content > 10 and last_bin_relerr < 0.3) or len(
                            new_binning
                        ) <= 2:
                            succesfull = True
                        else:
                            nb -= 1j
                else:
                    nb = 4j if "boosted" in ctags else 6j
                    bkgarr = np.sum(hnom[bidx], axis=0)["value"]
                    quant = wquant(bin_centers, bkgarr, np.r_[0:1:nb])
                    idx = np.digitize(quant, bin_edges) - 1
                    new_binning = np.r_[bin_edges[0], bin_edges[idx], bin_edges[-1]]

                # uniquify - no 0-width bins allowed
                new_binning = np.unique(new_binning)

                if c in binnings and is_valid:
                    p = binnings.lookup(c)
                    assert binnings[c] is None, f"multiple definitions of {p}"
                    binnings[p] = list(new_binning)

            return binnings
        else:
            return super(StatModelBase, cls).calculate_binnings(variable, h, task)

    @classmethod
    def trim_binning(cls, year, variable, category, bins, minl, task):
        if variable.startswith("dnn_"):
            info = f"trim binning - {variable}"
            info += f" - {year}"
            info += f" - '{category}'"
            newbins = [bins[0]] + bins[len(bins) - minl + 1 :]
            assert len(newbins) == minl
            info += f" to (from {len(bins)} to {len(newbins)}):"
            info += f" \n\t - Old: {bins}"
            info += f" \n\t - New: {newbins}"
            task.logger.info(info)
            bins = newbins
        return bins

    @classmethod
    def rebin(cls, variable, h, binnings, task):
        from tqdm.auto import tqdm

        if variable.name.startswith("dnn_"):
            binnings = AsteriskDict(binnings)
            hists = {}
            categories = [*h.axes["category"]]

            for c in tqdm(
                categories,
                unit="category",
                desc=f"rebin - {variable.name}",
                leave=False,
            ):
                # only rebin where we calculated binnings for
                if c in binnings:
                    binning = binnings[c]
                    # print info only for fit categories
                    if (
                        type(task).__name__ == "GroupCoffeaProcesses"
                        and "tt_fh" in h.axes["process"]
                    ):
                        tqdm.write(
                            f"rebin for fake non-closure estimation - {variable.name} in '{c}' to {binning}"
                        )
                    elif "fit" in (cat := task.analysis_inst.categories.get(c)).tags:
                        tqdm.write(f"rebin - {variable.name} in '{cat.label_short}' to {binning}")
                else:
                    # what to do with categories we didn't calculate binnings for?
                    # for now, use 10 equidistant bins
                    binning = np.r_[0:1:11j]

                hview = h.view()[:, h.axes[1].index(c)].copy()
                bin_centers = h.axes[variable.name].centers
                bin_edges = h.axes[variable.name].edges

                if set(binning) - set(bin_edges):
                    bad_binning = binning
                    binning = bin_edges[np.searchsorted(bin_centers, binning)]
                    if 1e-5 < np.max(np.abs(bad_binning - binning)):
                        info = f"Fixed binning misaligned for {c}:"
                        info += f" \n\t - Bad: {bad_binning}"
                        info += f" \n\t - Fix: {binning}"
                        tqdm.write(info)

                hnew = hist.Hist(
                    h.axes["process"],
                    h.axes["systematic"],
                    hist.axis.Variable(
                        binning,
                        name=variable.name,
                        label=variable.x_title,
                    ),
                    storage=hist.storage.Weight(),
                )

                loc = h.axes[variable.name].index
                ax = hnew.axes[variable.name]
                for bin in range(ax.size):
                    hnew[..., bin] = hview[..., loc(ax[bin][0]) : loc(ax[bin][1])].sum(axis=-1)

                hnew = smooth_systs(
                    hnew,
                    wrt_std=True,
                    chi2max=1,
                    shift_prefix=(
                        "Absolute",
                        "BBEC1",
                        "EC2",
                        "FlavorQCD",
                        "HF",
                        "RelativeBal",
                        "RelativeSample",
                        "UnclustEn",
                        "HemIssue",
                        "jer",
                        # raw dataset shifts
                        "TuneCP5",
                        "HDamp",
                        # top dataset shifts
                        "UnderlyingEvent",
                        "TopMass",
                        "GluonMove",
                        "QCDbased",
                        "erdON",
                        "MEPSMatchingScale",
                    ),
                )
                hists[c] = hnew
            return hists
        else:
            return super(StatModelBase, cls).rebin(variable, h, binnings, task)
