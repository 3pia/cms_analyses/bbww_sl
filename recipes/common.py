# -*- coding: utf-8 -*-

from warnings import warn
import logging

import numpy as np
import awkward as ak

from utils.tf_serve import autoClient
from processor.util import reduce_or, nano_object_overlap
import tasks.corrections.processors as corr_proc
from tasks.neutrino import NeutrinoReconstructionFit1

from tools.data import TensorMetadata as TM

logger = logging.getLogger(__name__)


def veto_taus(taus, fakeable_leptons):
    tau_mask = (
        (taus.pt > 20.0)
        & (abs(taus.eta) < 2.3)
        & (abs(taus.dxy) <= 1000.0)
        & (abs(taus.dz) <= 0.2)
        & (taus.idDecayModeNewDMs)
        & (reduce_or(*(taus.decayMode == m for m in [0, 1, 2, 10, 11])))
        & ((taus.idDeepTau2017v2p1VSjet >> 4 & 0x1) == 1)
        & ((taus.idDeepTau2017v2p1VSe >> 0 & 0x1) == 1)
        & ((taus.idDeepTau2017v2p1VSmu >> 0 & 0x1) == 1)
    )
    presel_taus = taus[tau_mask]
    clean_tau_fake = nano_object_overlap(presel_taus, fakeable_leptons, dr=0.3)
    return ~ak.any(clean_tau_fake, axis=-1)


def gen_match(lep, flavs=[1, 15]):
    return reduce_or(*(lep.genPartFlav == f for f in flavs))


class Base:
    common = ("energy", "x", "y", "z")
    debug_dataset = "GluGluToHHTo2B2WToLNu2J_node_SM"

    use_jet_parton_matching = False
    fat_jet_vars = ("tau1", "tau2", "tau3", "tau4", "msoftdrop")
    hl = (
        "m_hh_bregcorr",
        "pt_hh",
        "m_hbb_bregcorr",
        "pt_hbb",
        "m_hww",
        "pt_hww",
        "m_wjj",
        "pt_wjj",
        "m_wlep",
        "pt_wlep",
        "min_dr_lepbjets",
        "dphi_hbb_hww",
        "dphi_hbb_hwwvis",
        "dphi_met_lep",
        "dphi_met_hbb",
        "dphi_met_wjj",
        "dr_lep_hbb",
        "dr_lep_wjj",
        "min_dr_wjets",
        "min_dhi_wjets",
        "min_dr_bjets",
        "min_dphi_bjets",
        "ht",
        "smin",
        "vbf_pair_mass",
        "vbf_pairs_absdeltaeta",
        "lep_conept",
        "vbf_tag",
        "boosted_tag",
        "n_btag",
        # Event Shape Variables
        "sphericity",
        "sphericity_T",
        "aplanarity",
        "eventshape_C",
        "eventshape_D",
        "eventshape_Y",
        "foxwolfram1",
        "foxwolfram2",
        "foxwolfram3",
        "foxwolfram4",
        "foxwolfram5",
        "centrality",
        "centrality_jets",
        "eigenvalue1",
        "eigenvalue2",
        "eigenvalue3",
        "sphericity_met",
        "sphericity_T_met",
        "aplanarity_met",
        "eventshape_C_met",
        "eventshape_D_met",
        "eventshape_Y_met",
        "foxwolfram1_met",
        "foxwolfram2_met",
        "foxwolfram3_met",
        "foxwolfram4_met",
        "foxwolfram5_met",
        "centrality_met",
        "centrality_jets_met",
        "eigenvalue1_met",
        "eigenvalue2_met",
        "eigenvalue3_met",
    )

    param = ("year",)

    def obj_arrays(self, X, n, extra=()):
        assert 0 < n and n == int(n)
        cols = self.common + extra

        def pp(x):
            return x if n is True else ak.to_numpy(ak.fill_none(ak.pad_none(x, n, clip=True), 0))

        return np.stack(
            [pp(getattr(X, a)).astype(np.float32) for a in cols],
            axis=-1,
        )

    # fmt: off
    @property
    def tensors(self):
        common = self.common
        fat_jet_vars = self.fat_jet_vars
        hl = self.hl
        param = self.param
        return {
            "lep": TM("lep_cone", 1, common + ("pdgId", "charge"), np.float32, {"groups": ["multiclass", "eval", "input", "part"]}, label="Lepton"),
            "jet": TM("analysis_jets", 6, common + ("btagDeepFlavB",), np.float32, {"groups": ["multiclass", "eval", "input", "part"]}, label="AK4 Jets"),
            "fat": TM("good_fat_bjets", 1, common + fat_jet_vars, np.float32, {"groups": ["multiclass", "eval", "input", "part"]}, label="AK8 Jet"),
            "met": TM("metp4", 0, common, np.float32, {"groups": ["multiclass", "eval", "input", "part"]}, label="MET"),
            "nu": TM("neutrino", 0, common, np.float32, {"groups": ["multiclass", "eval", "input", "part"], "blackout": [np.s_[0:]]}, label="Neutrino"),
            "hl": TM(None, 0, hl, np.float32, {"groups": ["multiclass", "eval", "input"], "blackout": [np.s_[30:]]}, label="High-Level"),
            "param": TM(None, 0, param, np.float32, {"groups": ["multiclass", "eval", "input"]}, label="Parameters"),
            "eventnr": TM(None, 0, ["eventnr"], np.int64, {"groups": ["multiclass", "eval", "split"]}, label="Eventnumber"),
            "procid": TM(None, 0, ["procid"], np.int32, {"groups": ["multiclass", "class"]}, label="Process ID"),
        }
    # fmt: on

    def arrays(self, X):
        out = {}
        for name, tensor in self.tensors.items():
            id, n, vars, typ, aux = tensor

            if not (id is None or id in X.keys()):
                continue

            def preproc(x):
                return ak.to_numpy(
                    x if n == 0 else ak.fill_none(ak.pad_none(x, n, clip=True), 0),
                    allow_missing=False,
                )

            if id is None:
                vals = [preproc(X[var]) for var in vars]
                vals = np.stack(vals, axis=-1)
            elif id == "weights":
                vals = []
                for var in vars:
                    attr = "_weights"
                    if var.endswith(("Up", "Down")):
                        attr = "_modifiers"
                    vals.append(preproc(getattr(X[id], attr)[var]))
                vals = np.stack(vals, axis=-1)
            elif id == "pred":
                vals = X[id]
            else:
                vals = [preproc(getattr(X[id], var)) for var in vars]
                vals = np.stack(vals, axis=-1)
            # set all nans or infs to 0
            out[name] = np.nan_to_num(vals, nan=0.0, posinf=0.0, neginf=-0.0).astype(typ)
        return out

    def datasets_arrays(self, select_output, dataset):
        if dataset is None:
            return {}
        tensor = self.tensors.get("procid", ())
        assert tensor[:3] == (None, 0, ["procid"])
        (process,) = self.campaign_inst.datasets.get(dataset).processes.values
        return {"procid": select_output["const_arr"](process.id)[:, None].astype(tensor[3])}

    def get_pu_key(self, events):
        ds = self.get_dataset(events)
        if ds.is_data:
            return "data"
        else:
            return ds.name


class MCOnly:
    @classmethod
    def requires(cls, task):
        return task.base_requires(data_source="mc")


class Exporter:
    sep = "+"
    group = "default"
    skip_cats = {"fr", "nonprompt"}

    @classmethod
    def live_callback(cls, accumulator):
        ret = defaultdict(int)
        for cat, num in super().live_callback(accumulator).items():
            ret[cat.split(cls.sep, 1)[0]] += num
        return ret

    def select(self, events):
        out = next(super().select(events))
        out["categories"] = {
            key: val
            for key, val in out["categories"].items()
            if not (set(key.split("_")) & self.skip_cats)
        }

        # create inclusive training category
        incl = out["selection"].add(
            "incl",
            reduce_or(*(out["selection"].all(*v) for v in out["categories"].values())),
        )
        out["categories"] = {"all_inlusive_sr": ["incl"]}

        datasets = out.setdefault("datasets", {})
        datasets.setdefault(None, 1)
        for dsn, dsfactor in datasets.items():
            dataset = (
                self.get_dataset(events) if dsn is None else self.campaign_inst.datasets.get(dsn)
            )
            (process,) = dataset.processes.values

            w = process.xsecs[self.campaign_inst.ecm].nominal
            w *= self.campaign_inst.aux["lumi"]
            if w > 0:
                datasets[dsn] = dsfactor * w
            else:
                warn(f"xsec*lumi weight of {w} for datasets {dataset} is not positive")

        yield out

    @classmethod
    def save(cls, *args, **kwargs):
        kwargs["sep"] = cls.sep
        return super().save(*args, **kwargs)


class ExporterFlav(Exporter):
    sep = "+"
    group = "default"
    skip_cats = {"resolved", "boosted", "fr"}
    use_jet_parton_matching = True

    def full_indices(self, events, process="background"):
        def get_gen(pdgid, func=lambda x: x):
            return events.GenPart[
                (func(events.GenPart.pdgId) == pdgid)
                & events.GenPart.hasFlags(["fromHardProcess", "isLastCopy"])
            ]

        jets = events.Jet
        jets["flavour"] = jets.jetId * 0
        gen = jets.matched_gen

        has_hard_b = True
        if has_hard_b:
            if process == "signal":
                # get higgs bosons from hard process
                h = get_gen(25)

                # get b decaying higgs boson
                h_bb = h[(abs(h.children.pdgId) == 5).all()]

                # we know that we have exactly one
                assert np.all(h_bb.counts == 1)
                h_bb = h_bb[:, 0]

                # get bjets from this higgs boson
                b = h_bb.children
                assert np.all(b.counts == 2)
            else:  # for background
                b = get_gen(5, func=abs)
            # delta r matching
            is_b = (b[:, 0].delta_r(gen) < 0.4) | (b[:, 1].delta_r(gen) < 0.4)
        else:
            second_btag = jets.btagDeepFlavB[jets.btagDeepFlavB < jets.btagDeepFlavB.max()].max()
            is_b = jets.btagDeepFlavB >= second_btag

        has_hard_q = False
        if has_hard_q:
            # save indices
            jets["flavour"][is_b] = (jets[is_b].pt.argsort()).astype(np.int)

            # get w bosons from hard process
            w = get_gen(24, func=abs)

            # get hadronically decaying w boson
            w_jj = w[(abs(w.children.pdgId) <= 6).any()]

            # we know that we have exactly one
            assert np.all(w_jj.counts == 1)
            w_jj = w_jj[:, 0]

            # get jets from hadronically decaying w boson
            j = w_jj.children

            # delta r matching
            assert np.all(j.counts == 2)
            is_q = (j[:, 0].delta_r(gen) < 0.4) | (j[:, 1].delta_r(gen) < 0.4)
        else:
            second_pt_no_b = jets[~is_b].pt[jets[~is_b].pt < jets[~is_b].pt.max()].max()
            is_q = (jets.pt >= second_pt_no_b) & ~is_b
        # save indices
        jets["flavour"][is_q] = (jets[is_q].pt.argsort() + 2).astype(np.int)

        # mark rest of the jets
        other = ~(is_b.fillna(False) | is_q.fillna(False))
        jets["flavour"][other] = (jets[other].pt.argsort() + 4).astype(np.int)

    def add_jet_flavour(self, events):
        def get_gen(pdgid, func=lambda x: x):
            return events.GenPart[
                (func(events.GenPart.pdgId) == pdgid)
                & events.GenPart.hasFlags(["fromHardProcess", "isLastCopy"])
            ]

        jets = events.Jet
        jets["flavour"] = jets.jetId * 0
        gen = jets.matched_gen

        # get higgs bosons from hard process
        h = get_gen(25)

        # get b decaying higgs boson
        h_bb = h[(abs(h.children.pdgId) == 5).all()]

        # get bjets from this higgs boson
        b = h_bb.children
        gc, bc = gen.cross(b, nested=True).unzip()

        # delta r matching
        is_b = (gc.delta_r(bc) < 0.4).any().any()

        # set flavour
        jets["flavour"][is_b] = 2

        # get w bosons from hard process
        w = get_gen(24, func=abs)

        # get hadronically decaying w boson
        w_qq = w[(abs(w.children.pdgId) <= 6).any()]

        # get qets from hadronically decaying w boson
        q = w_qq.children
        gc, qc = gen.cross(q, nested=True).unzip()

        # delta r matching
        is_q = (gc.delta_r(qc) < 0.4).any().any()

        # set flavour
        jets["flavour"][is_q] = 1


class DNNBase:
    buid_dnn_cats = True

    def __init__(self, task):
        super().__init__(task)

        self.dnn = autoClient(self.dnn_model, remote=False)

    @property
    def dnn_model(self):
        raise NotImplementedError

    @property
    def multiclass_processes(self):
        multiclass_config = self.analysis_inst.aux["multiclass"]
        groups = multiclass_config.groups
        group = multiclass_config.group
        return groups[group]

    def category_variables(self, category):
        if "_dnn_node_" in category:
            return [v for v in self.variables if v.name.startswith("dnn_score_max")]
        else:
            return self.variables

    def variable_full_shifts(self, variable):
        return "shifts" in variable.tags

    def select(self, *args, **kwargs):
        for ret in super().select(*args, **kwargs):
            selection = ret["selection"]
            categories = ret["categories"]
            # DNN score:
            mask = reduce_or(*(selection.all(*cuts) for cuts in categories.values()))
            arr = self.arrays(ret)
            dnn_inputs = {}
            for k, v in arr.items():
                if k in self.tensors:
                    groups = self.tensors[k][-1].get("groups", [])
                    if "multiclass" in groups and "eval" in groups:
                        dnn_inputs[k] = v[mask]
            (pm,) = self.dnn(inputs=dnn_inputs).values()
            target_shape = (mask.sum(), len(self.multiclass_processes))
            assert pm.shape == target_shape, (pm.shape, target_shape)
            assert not np.any(np.isnan(pm)), "DNN produces nan values"

            pred = np.full(mask.shape[:1] + pm.shape[1:], np.nan, dtype=pm.dtype)
            pred[mask] = pm
            pmax = np.argmax(pred, axis=-1)
            catkeys = [c for c in categories.keys() if not (set(c.split("_")) & self.skip_cats)]
            if self.buid_dnn_cats:
                for i, c in enumerate(self.multiclass_processes.keys()):
                    m = pmax == i
                    cn = f"dnn_node_{c}"
                    selection.add(cn, m)
                    for k in catkeys:
                        cuts = categories[k]
                        cuts_dnn = cuts + [cn]
                        categories[f"{k}_{cn}"] = cuts_dnn
            ret["pred"] = pred
            yield ret


class NeutrinoBase:
    def __init__(self, task):
        super().__init__(task)
        neutrino_reco_path = "/net/scratch/cms/dihiggs/store/NeutrinoReconstructionFit1/prod27/optimizer_SGD/batch_size_150000/mc_H_MASS/model"
        self.client = autoClient(neutrino_reco_path, remote=False)
        self.neutrino_reco = lambda x: self.client.batched(x, batch_size=150_000)

        # heat neutrino reco
        logger.info("Preheating Neutrino reco")
        self.neutrino_reco(np.array([[1.0, 1.0, 1.0, 1.0, 1.0, 1.0]], dtype=np.float32))


class PUCount(Base, corr_proc.PUCount):
    memory = "1000MiB"


class HHCount(Base, corr_proc.HHCount):
    memory = "2000MiB"


class VJetsStitchCount(Base, corr_proc.VJetsStitchCount):
    memory = "1000MiB"


class VptSFCount(Base, corr_proc.VptSFCount):
    debug_dataset = "DYJetsToLL_M-10to50"
    memory = "1000MiB"
