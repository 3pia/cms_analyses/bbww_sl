# coding: utf-8
import os
import re
from functools import reduce
import warnings
import logging

import awkward as ak
from utils.util import pscan_vispa
import vector
import numpy as np
from operator import mul
from tqdm.auto import tqdm
import coffea.processor as processor
from coffea.processor.accumulator import set_accumulator, defaultdict_accumulator, dict_accumulator
from scipy.optimize import curve_fit
from scinum import Number, UP
from tasks.corrections.btag import BTagSF_reshape, BTagSubjetCorrections

from utils.bh5 import Histogram as Hist5
from processor.generator import GeneratorHelper
import processor.util as util
import processor.bbww as bbww
from processor.fake import ff
import tasks.corrections.processors as corr_proc
from tasks.corrections.fake import HistBinLookup
from utils.coffea import (
    BaseProcessor,
    ArrayExporter,
    TreeExporter,
    Histogramer,
    PackedSelection,
    Weights,
)
from utils.topvariations import TopSystematics, topvariations
from utils.plot import label, auto_ratio_ylim

from . import common
from .common import (
    veto_taus,
    gen_match,
    MCOnly,
    # need to be explicitly present in module:
    PUCount,
    HHCount,
    VJetsStitchCount,
    VptSFCount,
)

logger = logging.getLogger(__name__)


class Base(common.Base):
    @property
    def trigger(self):
        return {
            2016: {
                "e": {
                    "Ele27_WPTight_Gsf": all,
                    "Ele25_eta2p1_WPTight_Gsf": all,
                    "Ele27_eta2p1_WPLoose_Gsf": all,  # Heavily prescaled in run H
                },
                "mu": {
                    "IsoMu22": all,  # Prescaled since mid run E til the end of 2016
                    "IsoTkMu22": all,  # Prescaled since mid run E til the end of 2016
                    "IsoMu22_eta2p1": all,  # Introduced mid run B
                    "IsoTkMu22_eta2p1": all,  # Introduced mid run B
                    "IsoMu24": all,
                    "IsoTkMu24": all,
                },
            },
            2017: {
                "e": {
                    "Ele35_WPTight_Gsf": all,
                    "Ele32_WPTight_Gsf": all,  # Missing in runs B and C
                },
                "mu": {
                    "IsoMu24": all,  # Prescaled at high lumi
                    "IsoMu27": all,
                },
            },
            2018: {
                "e": {
                    "Ele32_WPTight_Gsf": all,
                },
                "mu": {
                    "IsoMu24": all,
                    "IsoMu27": all,
                },
            },
        }[int(self.year)]

    def select(self, events):
        output = self.accumulator.identity()

        # get meta infos
        dataset_key = tuple(events.metadata["dataset"])
        dataset_inst = self.get_dataset(events)

        if self.use_jet_parton_matching:
            self.add_jet_flavour(events)

        # inject variables
        events["Muon", "cone_pt"] = bbww.conept_mu(events.Muon)
        events["Electron", "cone_pt"] = bbww.conept_ele(events.Electron)
        events["Muon", "tight_cand"] = events.Muon.mediumId & (events.Muon.mvaTTH >= 0.5)
        events["Electron", "tight_cand"] = events.Electron.mvaTTH >= 0.3
        if dataset_inst.is_mc:
            events["Muon", "gen_matched"] = gen_match(events.Muon)
            events["Electron", "gen_matched"] = gen_match(events.Electron)

        # BTagSF injection
        #  Subjet
        BTagSubjetCorrections.prepare(events, self.corrections, wp="medium")

        # filter NaNs arising from broken events with broken MET
        # e.g.: debug_dataset, debug_uuids = "WJetsToLNu_HT-100To200", {"DB4EE882-D7FB-984A-A1E0-D3E3DADCD19D"}, 1st chunk
        # Event No: 54867
        goodMET = np.isfinite(events.MET.pt)
        if not np.all(goodMET):
            warnings.warn(f"{np.sum(~goodMET)}/{len(events)} MET events are NaN, dropping them")
            events = events[goodMET]

        # filter bad PDF
        if dataset_inst.is_mc:
            events = self.corrections["pdf"].filterBad(events)

        n_events = len(events)
        eventnr = ak.to_numpy(events.event)

        dataset_id = dataset_inst.id * np.ones(n_events)

        run = ak.flatten(events.run, axis=-1)
        lumi = ak.flatten(events.luminosityBlock, axis=-1)

        def const_arr(value):
            arr = np.broadcast_to(value, shape=(n_events,))
            arr.flags.writeable = False
            return arr

        # uuid - file finder debug variable
        uuid = self.get_lfn(events, default="").rsplit("/", 1)[-1].split(".", 1)[0].replace("-", "")
        uuid = [const_arr(int(uuid[i], 16) if i < len(uuid) else 17) for i in range(32)]

        dataset_id = dataset_inst.id * np.ones(n_events)

        weight_bad = const_arr(np.float32(np.nan))

        # early category definition
        common = [
            "lumimask",
            "met_filter",
            "ge_one_fakeable_leptons",
            "leq_one_tight_leps",
            "tau_veto",
            "low_mass_resonances",
            "zee_mass_window_cut",
            "zmumu_mass_window_cut",
        ]
        dim1 = {"e": ["ch_e"], "mu": ["ch_mu"]}
        dim2 = {
            "boosted": [">=1fatcleanedjet", ">=1fatjet", ">=1fatbjet"],
            "resolved_1b": [">=3jets", "==1bjets", "!>=1fatbjet"],
            "resolved_2b": [">=3jets", ">=2bjets", "!>=1fatbjet"],
        }
        dim3 = {"sr": ["sr"], "fr": ["fr"]}
        dim4 = {"prompt": ["gen_matched"], "nonprompt": ["!gen_matched"]}

        # load objects
        muons = events.Muon
        electrons = events.Electron
        taus = events.Tau

        # adjust sorting
        muons = muons[ak.argsort(muons.cone_pt, ascending=False)]
        electrons = electrons[ak.argsort(electrons.cone_pt, ascending=False)]

        # dataset shifts
        datasets = {None: 1}  # default: no additional datasets
        if dataset_inst.is_mc:
            if "hhreweigthing" in self.corrections:
                datasets.update(
                    self.corrections["hhreweigthing"].generate(
                        events, dataset_inst, self.analysis_inst
                    )
                )

        for unc, shift, met, jets, fatjets in self.jec_loop(events):
            # adjust sorting
            jets = jets[ak.argsort(jets.pt, ascending=False)]
            fatjets = fatjets[ak.argsort(fatjets.pt, ascending=False)]

            weights = Weights(n_events, dtype=np.float32, storeIndividual=self.individual_weights)
            selection = PackedSelection()
            # correct phi MET modulation
            met = self.corrections["metphi"](met, events.PV.npvs, dataset_inst)

            # start object cutflow
            output["object_cutflow"]["total muons"] += ak.sum(ak.num(muons))
            output["object_cutflow"]["total electrons"] += ak.sum(ak.num(electrons))
            output["object_cutflow"]["total jets"] += ak.sum(ak.num(jets))

            # muons
            presel_muons = bbww.presel_mu(muons)
            output["object_cutflow"]["preselected muons"] += ak.sum(ak.num(presel_muons))
            fakeable_muons = bbww.fakeable_mu(presel_muons, year=self.year)
            output["object_cutflow"]["fakeable muons"] += ak.sum(ak.num(fakeable_muons))

            # electrons
            presel_electrons_preclean = bbww.presel_ele(electrons)
            # preselected electrons need to be cleaned wrt preselected muons
            presel_electrons = presel_electrons_preclean[
                util.nano_object_overlap(presel_electrons_preclean, presel_muons, dr=0.3)
            ]
            output["object_cutflow"]["preselected electrons"] += ak.sum(ak.num(presel_electrons))
            fakeable_electrons = bbww.fakeable_ele(presel_electrons, year=self.year)
            output["object_cutflow"]["fakeable electrons"] += ak.sum(ak.num(fakeable_electrons))

            # build jet masks
            jID = "isLoose" if self.year == "2016" else "isTight"
            mask_jets_central = util.reduce_and(
                getattr(jets, jID),
                jets.pt >= 25.0,
                np.abs(jets.eta) <= 2.4,
            )
            mask_jets_vbf = util.reduce_and(
                getattr(jets, jID),
                jets.pt >= 30.0,
                np.abs(jets.eta) <= 4.7,
                util.reduce_or(
                    jets.pt >= 60,
                    np.abs(jets.eta) < 2.7,
                    np.abs(jets.eta) > 3.0,
                ),
            )
            mask_jets_puid_loose = util.reduce_or(
                ak.values_astype(((jets.puId >> 2) & 1), bool),
                (jets.pt > 50),
            )

            # apply mask & puID
            presel_jets = jets[mask_jets_central & mask_jets_puid_loose]
            presel_vbf_jets = jets[mask_jets_vbf & mask_jets_puid_loose]
            output["object_cutflow"]["presel jets"] += ak.sum(ak.num(presel_jets))

            # fat jets
            good_fat_jets = fatjets[
                util.reduce_and(
                    getattr(fatjets, jID),
                    fatjets.pt >= 200.0,
                    np.abs(fatjets.eta) <= 2.4,
                    fatjets.msoftdrop > 30,
                    fatjets.msoftdrop < 210,
                    (fatjets.tau2 / fatjets.tau1) <= 0.75,
                    ak.num(fatjets.subjets, axis=-1) == 2,
                    ak.all(fatjets.subjets.pt >= 20, axis=-1),
                    ak.all(np.abs(fatjets.subjets.eta) <= 2.4, axis=-1),
                )
            ]

            # create leptons
            fakeable_leptons = ak.with_name(
                ak.concatenate([fakeable_muons, fakeable_electrons], axis=1), "PtEtaPhiMCandidate"
            )
            lep = fakeable_leptons[ak.argsort(fakeable_leptons.cone_pt, ascending=False)[:, :1]]

            # tau_veto
            tau_veto = veto_taus(taus, fakeable_leptons)
            selection.add("tau_veto", tau_veto)

            # now clean all objects
            def cleanagainst_leptons(toclean, dr):
                return toclean[util.nano_object_overlap(toclean=toclean, cleanagainst=lep, dr=dr)]

            presel_jets = cleanagainst_leptons(presel_jets, 0.4)
            output["object_cutflow"]["clean jets"] += ak.sum(ak.num(presel_jets))

            presel_vbf_jets = cleanagainst_leptons(presel_vbf_jets, 0.4)
            good_fat_jets = cleanagainst_leptons(good_fat_jets, 0.8)

            output["n_events"][dataset_key] = n_events

            # met filter
            if dataset_inst.is_data:
                met_filter = self.campaign_inst.aux["met_filters"]["data"]
            else:
                met_filter = self.campaign_inst.aux["met_filters"]["mc"]

            # selection

            # select one lepton
            selection.add("ge_one_fakeable_leptons", ak.num(fakeable_leptons) >= 1)

            selection.add("leq_one_tight_leps", ak.sum(fakeable_leptons.tight_cand, axis=-1) <= 1)

            sr = ak.any(lep.tight_cand, axis=-1)
            selection.add("sr", sr)
            selection.add("fr", ~sr)

            met_filter_mask = util.nano_mask_and(events.Flag, met_filter)
            selection.add("met_filter", met_filter_mask)

            if dataset_inst.is_data:
                selection.add("lumimask", self.corrections["lumimask"](events))
            else:
                selection.add("lumimask", np.ones(n_events, dtype=bool))

            # select at least one bjet
            n_btag = ak.sum(presel_jets.btagDeepFlavB >= bbww.btag_wp[self.year]["medium"], axis=-1)
            selection.add("==1bjets", n_btag == 1)
            selection.add(">=2bjets", n_btag >= 2)

            # remove J/Psi and co
            presel_leptons = ak.with_name(
                ak.concatenate([presel_muons, presel_electrons_preclean], axis=1),
                "PtEtaPhiMCandidate",
            )
            presel_leptons_tpls = ak.combinations(
                presel_leptons,
                n=2,
                replacement=False,
                axis=-1,
                fields=["l1", "l2"],
            )
            selection.add(
                "low_mass_resonances",
                ak.all((presel_leptons_tpls.l1 + presel_leptons_tpls.l2).mass > 12, axis=-1),
            )

            # mll cut for ee and mumu
            muon_tpls = ak.combinations(
                presel_muons,
                n=2,
                replacement=False,
                axis=-1,
                fields=["m1", "m2"],
            )
            dimuon = muon_tpls.m1 + muon_tpls.m2
            zmumu_mass_window_cut = util.reduce_or(
                np.abs(dimuon.mass - 91.1876) > 10.0,
                dimuon.charge != 0,
            )
            selection.add("zmumu_mass_window_cut", ak.all(zmumu_mass_window_cut, axis=-1))

            electron_tpls = ak.combinations(
                presel_electrons,
                n=2,
                replacement=False,
                axis=-1,
                fields=["e1", "e2"],
            )
            dielectron = electron_tpls.e1 + electron_tpls.e2
            zee_mass_window_cut = util.reduce_or(
                abs(dielectron.mass - 91.1876) > 10.0,
                dielectron.charge != 0,
            )
            selection.add("zee_mass_window_cut", ak.all(zee_mass_window_cut, axis=-1))

            # redundant to boosted_tag but needed for BTagSFNorm, dropping btag cuts
            selection.add(
                ">=1fatjet", ak.any(ak.any(good_fat_jets.subjets.pt >= 30, axis=-1), axis=-1)
            )

            good_fat_bjets = good_fat_jets[
                ak.any(
                    util.reduce_and(
                        good_fat_jets.subjets.btagDeepB
                        >= self.campaign_inst.aux["working_points"]["subjet_deepb"]["medium"],
                        good_fat_jets.subjets.pt >= 30,
                    ),
                    axis=-1,
                )
            ]
            selection.add(">=1fatbjet", ak.num(good_fat_bjets) >= 1)

            selection.add(">=3jets", ak.num(presel_jets) >= 3)
            jet_no_fat_overlap = presel_jets[
                util.nano_object_overlap(
                    toclean=presel_jets, cleanagainst=good_fat_bjets[:, :1], dr=1.2
                )
            ]
            selection.add(">=1fatcleanedjet", ak.num(jet_no_fat_overlap) >= 1)

            # boosted selection/tag
            boosted_tag = ak.num(good_fat_bjets) > 0

            # select 3rd or 4th or higher jet in btag score sorting
            w_dijet_cand = presel_jets[ak.argsort(presel_jets.btagDeepFlavB, ascending=False)][
                :, 2:
            ]
            # resort again to pt
            # only takes two leading pT not b tagged
            w_dijet_cand = w_dijet_cand[ak.argsort(w_dijet_cand.pt, ascending=False)][:, :2]
            wj_tpl = ak.combinations(
                w_dijet_cand,
                n=2,
                replacement=False,
                axis=1,
                fields=["lead", "sub"],
            )
            wj_pairs = wj_tpl.lead + wj_tpl.sub
            # take those which are in Wmass window
            wmass_cut = abs(wj_pairs.mass - 80.4) < 15.0
            wj_cand = wj_tpl[wmass_cut]
            whadjets = ak.concatenate([wj_cand.lead, wj_cand.sub], axis=1, merge=False)

            # vbf cleaning
            presel_vbf_jets = presel_vbf_jets[
                util.reduce_or(
                    util.nano_object_overlap(
                        toclean=presel_vbf_jets,
                        cleanagainst=presel_jets[
                            ak.argsort(presel_jets.btagDeepFlavB, ascending=False)
                        ][:, :2],
                        dr=0.8,
                    )
                    & util.nano_object_overlap(
                        toclean=presel_vbf_jets,
                        cleanagainst=whadjets,  # TODO: also here [..., :1] ?
                        dr=0.8,
                    )
                    & ~boosted_tag,
                    util.nano_object_overlap(
                        toclean=presel_vbf_jets,
                        cleanagainst=good_fat_bjets[:, :1],
                        dr=1.2,
                    )
                    & boosted_tag,
                )
            ]

            # vbf selection/tag
            vbf_pairs = ak.combinations(
                presel_vbf_jets,
                n=2,
                replacement=False,
                axis=1,
                fields=["j1", "j2"],
            )
            vbf_dijets = vbf_pairs.j1 + vbf_pairs.j2
            vbf_pairs_absdeltaeta = np.abs(vbf_pairs.j1.eta - vbf_pairs.j2.eta)
            vbf_mask = util.reduce_and(
                vbf_dijets.mass > 500,
                vbf_pairs_absdeltaeta > 3.0,
            )
            vbf_dijets = vbf_dijets[vbf_mask]
            vbf_pairs_absdeltaeta = vbf_pairs_absdeltaeta[vbf_mask]
            sort = ak.argsort(vbf_dijets.mass, ascending=False)
            vbf_dijets = vbf_dijets[sort]
            vbf_pairs_absdeltaeta = vbf_pairs_absdeltaeta[sort]

            vbf_tag = np.asarray(ak.num(vbf_dijets) > 0, int)
            selection.add("vbf_tag", ak.num(vbf_dijets) > 0)
            selection.add("ggf_tag", ak.num(vbf_dijets) == 0)

            # trigger
            trigger = util.Trigger(events.HLT, self.trigger, dataset_inst)

            # standard
            ch_mu = (
                ak.any(lep.cone_pt > 25.0, axis=-1)
                & trigger.get("mu")
                & ak.any(abs(lep.pdgId) == 13, axis=-1)
            )
            ch_e = (
                ak.any(lep.cone_pt > 32.0, axis=-1)
                & trigger.get("e")
                & ak.any(abs(lep.pdgId) == 11, axis=-1)
            )

            selection.add("ch_mu", ch_mu)
            selection.add("ch_e", ch_e)

            gen_matched = (
                ak.any(lep.gen_matched, axis=-1)
                if dataset_inst.is_mc
                else np.ones(n_events, dtype=bool)
            )
            selection.add("gen_matched", gen_matched)

            # build categories
            categories = {
                "_".join([dim1_key, dim2_key, dim3_key, dim4_key]): common
                + dim1_value
                + dim2_value
                + dim3_value
                + dim4_value
                for dim1_key, dim1_value in dim1.items()
                for dim2_key, dim2_value in dim2.items()
                for dim3_key, dim3_value in dim3.items()
                for dim4_key, dim4_value in dim4.items()
            }

            # weights
            if dataset_inst.is_data:
                output["sum_gen_weights"][dataset_key] = 0.0
                output["count_bad_gen_weights"][dataset_key] = 0.0
            if dataset_inst.is_mc:
                self.corrections["pdf"](events, weights, clip=10, relative=True)

                gh = GeneratorHelper(events, weights)
                gh.PSWeight(lfn=self.get_lfn(events))
                gh.ScaleWeight()

                # NOTE: all prior (nominal + their variations) weights are included in sum_gen_weight!
                gh.gen_weight(output, dataset_key, datasets)

                weights.add(
                    "pileup",
                    **self.corrections["pileup"](
                        pu_key=self.get_pu_key(events), nTrueInt=events.Pileup.nTrueInt
                    ),
                )

                # L1 ECAL prefiring
                if self.year in ("2016", "2017"):
                    weights.add(
                        "l1_ecal_prefiring",
                        events.L1PreFiringWeight.Nom,
                        weightUp=events.L1PreFiringWeight.Up,
                        weightDown=events.L1PreFiringWeight.Dn,
                    )

                if dataset_inst.name.startswith("TTTo"):
                    w_toppt = ak.to_numpy(util.top_pT_reweighting(events.GenPart)).squeeze()
                    weights.add(
                        "top_pT_reweighting",
                        w_toppt,
                        weightUp=w_toppt**2,  # w**2
                        weightDown=ak.ones_like(w_toppt),  # w = 1.
                    )

                # lepton sf
                e_corr = self.corrections["electron"]
                mu_corr = self.corrections["muon"]

                # electrons
                # fmt: off
                ## id and loose tth id
                mask = (abs(lep.pdgId) == 11) & lep.gen_matched
                loose_1 = e_corr[f"electron_Tallinn_id_loose_01_EGamma_SF2D"](abs(lep.eta), lep.pt)[mask]   # using pT
                loose_1_err = e_corr[f"electron_Tallinn_id_loose_01_EGamma_SF2D_error"](abs(lep.eta), lep.pt)[mask] # using pT
                loose_2 = e_corr[f"electron_Tallinn_id_loose_02_EGamma_SF2D"](abs(lep.eta), lep.pt)[mask] # using pT
                loose_2_err = e_corr[f"electron_Tallinn_id_loose_02_EGamma_SF2D_error"](abs(lep.eta), lep.pt)[mask] # using pT

                if self.year in ("2016", "2017"):
                    # for 2016 and 2017 the reco eff is split into a low and high pt region
                    reco_low = e_corr[f"electron_Tallinn_reco_low_EGamma_SF2D"](lep.eta, lep.pt)[mask & (lep.pt < 20)] # using pT
                    reco_low_err = e_corr[f"electron_Tallinn_reco_low_EGamma_SF2D_error"](lep.eta, lep.pt)[mask & (lep.pt < 20)] # using pT
                    reco_high = e_corr[f"electron_Tallinn_reco_high_EGamma_SF2D"](lep.eta, lep.pt)[mask & (lep.pt >= 20)] # using pT
                    reco_high_err = e_corr[f"electron_Tallinn_reco_high_EGamma_SF2D_error"](lep.eta, lep.pt)[mask & (lep.pt >= 20)] # using pT
                    electron_id_nom, electron_id_down, electron_id_up = (
                        reduce(
                            mul,
                            [
                                ak.prod(sf, axis=-1) for sf in [
                                    reco_low + sign * reco_low_err,
                                    reco_high + sign * reco_high_err,
                                    loose_1 + sign * loose_1_err,
                                    loose_2 + sign * loose_2_err,
                                ]
                            ]
                        )
                        for sign in (0, -1, +1)
                    )
                elif self.year == "2018":
                    reco = e_corr[f"electron_Tallinn_reco_EGamma_SF2D"](lep.eta, lep.pt)[mask] # using pT
                    reco_err = e_corr[f"electron_Tallinn_reco_EGamma_SF2D_error"](lep.eta, lep.pt)[mask] # using pT
                    electron_id_nom, electron_id_down, electron_id_up = (
                        reduce(
                            mul,
                            [
                                ak.prod(sf, axis=-1) for sf in [
                                    reco + sign * reco_err,
                                    loose_1 + sign * loose_1_err,
                                    loose_2 + sign * loose_2_err,
                                ]
                            ]
                        )
                        for sign in (0, -1, +1)
                    )
                # fmt: on
                weights.add(
                    "electron_id_loose",
                    electron_id_nom,
                    weightUp=electron_id_up,
                    weightDown=electron_id_down,
                    shift=False,
                )

                # fmt: off
                ## tight tth id - only uncertainty
                mask = (abs(lep.pdgId) == 11) & lep.gen_matched & lep.tight_cand
                up = ak.prod(e_corr["electron_Tallinn_tth_tight_error_histo_eff_data_max"](abs(lep.eta), lep.pt)[mask], axis=-1)  # using pT
                down = ak.prod(e_corr["electron_Tallinn_tth_tight_error_histo_eff_data_min"](abs(lep.eta), lep.pt)[mask], axis=-1)  # using pT
                weights.add(
                    "electron_tth_tight",
                    np.ones(n_events),
                    weightUp=up,
                    weightDown=down,
                    shift=False,
                )

                ## relaxed tth id
                prefix = "electron_Tallinn_tth_relaxed_EGamma_SF2D"
                weights.add(
                    "electron_tth_relaxed",
                    ak.prod(e_corr[f"{prefix}"](abs(lep.eta), lep.pt)[mask], axis=-1),  # using pT
                    weightUp=ak.prod(e_corr[f"{prefix}_Up"](abs(lep.eta), lep.pt)[mask], axis=-1),  # using pT
                    weightDown=ak.prod(e_corr[f"{prefix}_Down"](abs(lep.eta), lep.pt)[mask], axis=-1),  # using pT
                )
                # fmt: on

                # muons
                ## id&iso
                mask = (abs(lep.pdgId) == 13) & lep.gen_matched
                prefix = "muon_Tallinn_idiso_loose_EGamma_SF2D"
                weights.add(
                    "muon_idiso_loose",
                    ak.prod(mu_corr[f"{prefix}"](abs(lep.eta), lep.pt)[mask], axis=-1),  # using pT
                    weightUp=ak.where(
                        ak.any(mask, axis=-1),
                        ak.prod(
                            mu_corr[f"{prefix}_error"](abs(lep.eta), lep.pt)[mask], axis=-1
                        ),  # using pT
                        0,
                    ),
                    weightDown=None,
                    shift=True,
                )

                # fmt: off
                ## tight tth id - only uncertainty
                mask = (abs(lep.pdgId) == 13) & lep.gen_matched & lep.tight_cand
                up = ak.prod(mu_corr["muon_Tallinn_tth_tight_error_histo_eff_data_max"](abs(lep.eta), lep.pt)[mask], axis=-1)
                down = ak.prod(mu_corr["muon_Tallinn_tth_tight_error_histo_eff_data_min"](abs(lep.eta), lep.pt)[mask], axis=-1)
                weights.add(
                    "muon_tth_tight",
                    np.ones(n_events),
                    weightUp=up,
                    weightDown=down,
                    shift=False,
                )

                ## relaxed tth id
                prefix = f"muon_Tallinn_tth_relaxed_EGamma_SF2D"
                weights.add(
                    f"muon_tth_relaxed",
                    ak.prod(mu_corr[f"{prefix}"](abs(lep.eta), lep.pt)[mask], axis=-1),  # using pT
                    weightUp=ak.prod(mu_corr[f"{prefix}_Up"](abs(lep.eta), lep.pt)[mask], axis=-1),  # using pT
                    weightDown=ak.prod(mu_corr[f"{prefix}_Down"](abs(lep.eta), lep.pt)[mask], axis=-1), # using pT
                )
                # fmt: on

                # trigger sf (based on cone_pt)
                lep_pt = util.normalize(lep.pt, pad=True)
                lep_abseta = np.abs(util.normalize(lep.eta, pad=True))
                for name, ch in [
                    ("muon", ch_mu),
                    ("electron", ch_e),
                ]:
                    prefix = f"Tallinn_single_{name}_trigger"
                    corr_trig = self.corrections["trigger"]["hist"]
                    nominal = corr_trig[f"{prefix}_nominal"](lep_abseta, lep_pt)
                    up = corr_trig[f"{prefix}_up"](lep_abseta, lep_pt)
                    down = corr_trig[f"{prefix}_down"](lep_abseta, lep_pt)
                    weights.add(
                        f"trigger_{name}_sf",
                        ak.where(ch, nominal, 1),
                        weightUp=ak.where(ch, up, 1),
                        weightDown=ak.where(ch, down, 1),
                    )

                # https://twiki.cern.ch/twiki/bin/viewauth/CMS/BTagSFMethods
                # AK8 subjet Method 1a
                BTagSubjetCorrections.apply(self, good_fat_jets.subjets, weights, wp="medium")
                # AK4 Jet reshape SF
                BTagSF_reshape(self, events, weights, presel_jets, shift, unc)

                if "vjets" in self.corrections:
                    self.corrections["vjets"](dataset_inst, events, weights)

                self.corrections["jetpuid"]["loose"](
                    cleanagainst_leptons(jets[mask_jets_central | mask_jets_vbf], dr=0.4), weights
                )

            # fake background estimation (is_estimation) and nc correction
            fake_corr = self.corrections["fake"]
            for name, pdgid, identifier, is_estimation in [
                ("fake_electrons", 11, "FR_mva030_el_data_comb", True),
                ("fake_muons", 13, "FR_mva050_mu_data_comb", True),
                ("fake_electrons_nc", 11, "FR_mva030_el_data_comb_QCD_fakes", False),
                ("fake_muons_nc", 13, "FR_mva050_mu_data_comb_QCD_fakes", False),
            ]:
                prefix = f"Tallinn_fakerates_sl_{identifier}"
                mask = (abs(lep.pdgId) == pdgid) & ~lep.tight_cand
                if is_estimation:
                    if dataset_inst.is_mc:
                        mask = mask * lep.gen_matched
                else:
                    if dataset_inst.is_mc:
                        mask = mask * ~lep.gen_matched
                    else:
                        continue

                fake_rate = fake_corr[f"{prefix}"](lep.cone_pt, abs(lep.eta))
                fake_rate_err = fake_corr[f"{prefix}_error"](lep.cone_pt, abs(lep.eta))

                # nominal fake factors
                nominal = ak.prod(ff(fake_rate[mask]), axis=-1)
                weights.add(name, nominal)

                # bin dependent uncertainties
                up = fake_rate + fake_rate_err
                down = fake_rate - fake_rate_err

                fake_lookup = HistBinLookup(fake_corr[prefix])
                for unc_name, unc_mask_func in fake_lookup.bin_masks:
                    unc_name = f"{name}:{unc_name}"
                    unc_mask = mask * unc_mask_func(lep.cone_pt, abs(lep.eta))

                    def rel_shift(fake_factor):
                        return ak.where(
                            ak.any(unc_mask, axis=-1),
                            fake_factor / nominal,
                            1.0,
                        )

                    weights.add(
                        unc_name,
                        ak.ones_like(nominal),
                        weightUp=rel_shift(ak.prod(ff(up)[unc_mask], axis=-1)),
                        weightDown=rel_shift(ak.prod(ff(down)[unc_mask], axis=-1)),
                    )

            # for shape in list(weights._modifiers.keys())[54:78]:
            #     ind = np.where(weights._modifiers[shape]!=1)
            #     print(shape, len(ind), weights._modifiers[shape][ind])

            ##################
            # reconstruction #
            ##################

            sync_electrons = presel_electrons[
                ak.argsort(presel_electrons.cone_pt, ascending=False)
            ][:, :2]
            sync_muons = presel_muons[ak.argsort(presel_muons.cone_pt, ascending=False)][..., :2]

            lep_cone = ak.zip(
                dict(
                    pt=lep.cone_pt,
                    eta=lep.eta,
                    phi=lep.phi,
                    mass=lep.mass,
                    pdgId=lep.pdgId,
                    charge=lep.charge,
                ),
                with_name="PtEtaPhiMCandidate",
            )
            lepp4 = lep_cone.sum()

            b_sort = ak.argsort(presel_jets.btagDeepFlavB, ascending=False)
            bjets = presel_jets[b_sort[:, :2]]
            lightjets = presel_jets[b_sort[:, 2:]]
            lightjets = lightjets[ak.argsort(lightjets.pt, ascending=False)]
            wjets = lightjets[:, :2]
            analysis_jets = ak.concatenate([bjets, lightjets[:, :4]], axis=1, merge=False)
            metp4 = util.get_metp4(met)

            hww_visible = lep_cone + wjets.sum(axis=-1)
            neutrino = metp4
            # neutrino = self.neutrino_reco(
            #     np.stack(
            #         (
            #             util.normalize(hww_visible.energy, pad=True),
            #             util.normalize(hww_visible.x, pad=True),
            #             util.normalize(hww_visible.y, pad=True),
            #             util.normalize(hww_visible.z, pad=True),
            #             util.normalize(metp4.x),
            #             util.normalize(metp4.y),
            #         ),
            #         axis=-1,
            #     )
            # )
            # neutrino = vector.arr(
            #     {
            #         "x": neutrino["x"],
            #         "y": neutrino["y"],
            #         "z": neutrino["z"],
            #         "M": 0 * neutrino["x"],
            #     }
            # )

            hbb = bjets.sum()
            wjj = wjets.sum()
            wlep = lepp4 + metp4
            hww = wjj + wlep
            hh = hbb + hww

            m_hh_bregcorr = (
                util.bregcorr(bjets).sum() + util.bregcorr(wjets).sum() + lepp4 + metp4
            ).mass
            pt_hh = hh.pt

            m_hbb_bregcorr = util.normalize(util.bregcorr(bjets).sum().mass)
            pt_hbb = hbb.pt

            m_hww = (wjj + wlep).mass
            pt_hww = hww.pt

            m_wjj = wjj.mass
            pt_wjj = wjj.pt

            m_wlep = wlep.mass
            pt_wlep = wlep.pt

            (min_dr_lepbjets,) = util.min_dr_part1_part2(lep_cone, bjets, getn=1)

            dphi_hbb_hww = abs(hbb.delta_phi(hww))
            dphi_hbb_hwwvis = abs(hbb.delta_phi(wjj + lepp4))

            dphi_met_lep = abs(lepp4.delta_phi(metp4))
            dphi_met_hbb = abs(metp4.delta_phi(hbb))
            dphi_met_wjj = abs(metp4.delta_phi(wjj))

            dr_lep_hbb = lepp4.delta_r(hbb)
            dr_lep_wjj = lepp4.delta_r(wjj)

            min_dr_wjets = util.min_dr(wjets)
            min_dhi_wjets = util.min_dphi(wjets)

            min_dr_bjets = util.min_dr(bjets)
            min_dphi_bjets = util.min_dphi(bjets)

            ht = util.get_ht(presel_jets)

            vis = hbb + wjj + lepp4
            vis_pt = vis.pt
            vis_m = vis.mass
            vis_et = np.sqrt(vis_m**2 + vis_pt**2)
            met_et = metp4.energy
            smin = np.sqrt(vis_m**2 + 2 * (vis_et * met_et - (vis.x * metp4.x + vis.y * metp4.y)))

            vbf_pair_mass = util.normalize(vbf_dijets.mass, pad=True, fill=0)
            vbf_pairs_absdeltaeta = util.normalize(vbf_pairs_absdeltaeta, pad=True, fill=0)

            lep_conept = util.normalize(lep_cone.pt, pad=True)

            eventshapes_met = util.get_eventshape_variables(
                met_or_nu=metp4,
                leps=lep_cone,
                jets=presel_jets,  # use analysis_jets here?
            )
            sphericity_met = eventshapes_met.sphericity
            sphericity_T_met = eventshapes_met.sphericity_T
            aplanarity_met = eventshapes_met.aplanarity
            eventshape_C_met = eventshapes_met.C
            eventshape_D_met = eventshapes_met.D
            eventshape_Y_met = eventshapes_met.Y
            foxwolfram1_met = eventshapes_met.foxwolfram[0]
            foxwolfram2_met = eventshapes_met.foxwolfram[1]
            foxwolfram3_met = eventshapes_met.foxwolfram[2]
            foxwolfram4_met = eventshapes_met.foxwolfram[3]
            foxwolfram5_met = eventshapes_met.foxwolfram[4]
            centrality_met = eventshapes_met.centrality
            centrality_jets_met = eventshapes_met.centrality_jets
            eigenvalue1_met = eventshapes_met.eigenvalues[:, 0]
            eigenvalue2_met = eventshapes_met.eigenvalues[:, 1]
            eigenvalue3_met = eventshapes_met.eigenvalues[:, 2]

            # these used to be the event shape variables reconstructed with the neutrino
            # neutrino reco is removed now, these variables should follow
            sphericity = sphericity_met
            sphericity_T = sphericity_T_met
            aplanarity = aplanarity_met
            eventshape_C = eventshape_C_met
            eventshape_D = eventshape_D_met
            eventshape_Y = eventshape_Y_met
            foxwolfram1 = foxwolfram1_met
            foxwolfram2 = foxwolfram2_met
            foxwolfram3 = foxwolfram3_met
            foxwolfram4 = foxwolfram4_met
            foxwolfram5 = foxwolfram5_met
            centrality = centrality_met
            centrality_jets = centrality_jets_met
            eigenvalue1 = eigenvalue1_met
            eigenvalue2 = eigenvalue2_met
            eigenvalue3 = eigenvalue3_met

            year = const_arr(int(self.year))
            if dataset_inst.is_mc:
                (process,) = dataset_inst.processes.values
                procid = const_arr(process.id)
            else:
                procid = const_arr(self.analysis_inst.processes.get("data").id)

            yield locals()


class DNNBase(common.DNNBase):
    """
    DNN Model
    - Training: https://git.rwth-aachen.de/3pia/cms_analyses/common/-/issues/300
    - Upload: not yet
    - Note: Fixed Lepton SF
    """

    dnn_model = "/net/scratch/cms/dihiggs/store/bbww_sl/MulticlassStitched/prod35/export_version_0/training_version_1/lbn_particles_12/ResNet/jump_2/layers_3/nodes_256/activation_relu/batch_norm_1/dropout_0.0/l2_1e-08/lossdef_default/optimizer_adam/learning_rate_1.00e-03/default_7/years_2016_2017_2018/model_default/cw_0p125_0p125_1_1_1_1_1/model"


class Processor(DNNBase, Base, Histogramer):
    jes_shifts = True
    dataset_shifts = True
    memory = "3000MiB" if jes_shifts else "2500MiB"
    skip_cats = set()

    # output = "issue_300.coffea"

    debug_dataset = "GluGluToHHTo2B2VLNu2J_node_cHHH1"
    # debug_dataset = "data_D_mu"
    # debug_uuids = ["F6A681A3-42F5-EF4F-A752-A37E03D60A40"]
    # debug_dataset = "DYJetsToLL_M-10to50"
    # debug_uuids = ["567332A7-A3B6-F64D-B089-32E53B924B97"]

    @classmethod
    def group_processes(cls, hists, task):
        def fakes(cat):
            if "_fr_prompt" in cat:
                return cat.replace("_fr", "_sr")

        def fakes_skip_mc(c):
            if c == "tt_fh":
                return False
            if task.analysis_inst.processes.get(c).has_parent_process(
                task.analysis_inst.processes.get("diHiggs")
            ):
                return False
            return True

        def data_syst(syst):
            return (syst.startswith("fake_") and "nc" not in syst) or any(
                syst.startswith(top_systematic) for top_systematic in TopSystematics
            )

        def tri_shape(x, n):
            return np.maximum(0, (x - 1 / n) * (n / (n - 1)))

        def apply_ncc(h, ncc_proc="tt_fh"):
            cats = set({})
            procs = set({})
            for cat in h.axes["category"]:
                c = re.search("^(e|mu)_(.+)_[sf]r_nonprompt(.*)$", cat)
                if c is not None:
                    cats.add(c.group(2))
                    procs.add(c.group(3))

            # add placeholders for the non-closure uncertainties
            fake_nc_shift_name = lambda shift, flav, dire: f"fake_nc_{shift}_{flav}{dire}"
            nc_shifts = [
                fake_nc_shift_name(shift, flavour, direction)
                for shift in ["nom", "slope"]
                for flavour in ["e", "mu"]
                for direction in ["Up", "Down"]
            ]
            h += Hist5.regrow(
                h,
                {
                    "process": ["fakes"],
                    "systematic": nc_shifts,
                },
                copy=False,
            )

            axes = h.axes[:3]
            assert axes.name == ("process", "category", "systematic")
            hv = h.view()
            x = h.axes[-1].centers

            # set fake shifts to fake nominal
            hv[axes[0].index("fakes"), :, axes[2].index(nc_shifts)] = hv[
                axes[0].index("fakes"), :, axes[2].index("nominal")
            ]

            # we have to group to 'all' to calculate the correct binnings here...
            binnings = task.StatModel.calculate_binnings(
                variable,
                cls.regroup(h, groups={"all": "^(e|mu)"}),
                task,
            )
            rebinned = task.StatModel.rebin(variable, h, binnings, task)

            for cat in cats:
                for proc in procs:
                    flavours = ("e", "mu")
                    for flavour in flavours:
                        region = lambda x: f"{flavour}_{cat}_{x}_nonprompt{proc}"
                        mc_fakes_hist = rebinned[region("sr")][ncc_proc, "nominal", :]
                        mc_closure_hist = rebinned[region("fr")][ncc_proc, "nominal", :]

                        assert np.all(
                            mc_fakes_hist.axes[-1].centers == mc_closure_hist.axes[-1].centers
                        ), "The histograms mc_fakes and mc_closure must have the same centers, did you use the binning json?"

                        rebinned_x = mc_fakes_hist.axes[-1].centers

                        mc_fakes = Number(
                            np.clip(mc_fakes_hist.view()["value"], a_min=0, a_max=np.inf),
                            np.sqrt(mc_fakes_hist.view()["variance"]),
                        )
                        mc_closure = Number(
                            np.clip(mc_closure_hist.view()["value"], a_min=0, a_max=np.inf),
                            np.sqrt(mc_closure_hist.view()["variance"]),
                        )

                        # get ratio, ignore invalids because they are not used anyway
                        with np.errstate(divide="ignore", invalid="ignore"):
                            r = mc_fakes / mc_closure

                        # remove unphysical values: infinities or no uncertainties (happens only for 0+-0, which breaks the fit)
                        good = np.isfinite(r.nominal)
                        good &= r.u(direction=UP) > 0

                        mc_fakes_good = mc_fakes.nominal[good]
                        mc_closure_good = mc_closure.nominal[good]
                        rebinned_x_good = rebinned_x[good]

                        # get center of gravity
                        with np.errstate(divide="ignore", invalid="ignore"):
                            cog = np.sum(mc_fakes_good * rebinned_x_good) / np.sum(mc_fakes_good)
                        if cog == 0 or not np.isfinite(cog):
                            cog = 0.5

                        # get ratio of integrals, ignore invalids because they are not used anyway
                        with np.errstate(divide="ignore", invalid="ignore"):
                            nom = np.sum(mc_fakes_good) / np.sum(mc_closure_good)
                        if not np.isfinite(nom):
                            logger.warning(
                                f"Nominal non-closure in {region('')} not finite. Use 100% uncertainty."
                            )
                            nom = np.nan_to_num(nom, nan=2, posinf=2, neginf=2)

                        if any(ss in proc for ss in ("HHGluGlu", "HHVBF")):
                            logger.info(f"Fake NCC: Only nominal unc. in region {region('')}.")
                            fun = lambda x, nom, slope: nom + (x - cog)
                            slope = 0
                        else:
                            if sum(good) < 2:
                                logger.warning(f"Fake NCC: Only nominal in region {region('')}.")
                                continue
                            fun = lambda x, nom, slope: nom + slope * (x - cog)

                            popt, pcov = curve_fit(
                                fun,
                                rebinned_x[good],
                                r.nominal[good] / nom,
                                sigma=r.u(direction=UP)[good] * r.nominal[good] / nom,
                                absolute_sigma=True,
                            )

                            slope = popt[1]

                            # plotting fake non-closure corrections
                            target = task.output()
                            ncc_path = os.path.join(
                                target.parent.path, "fake_ncc", f"{region('sr')}.pdf"
                            )
                            ncc_target = type(target)(ncc_path)
                            ncc_target.parent.touch()

                            from matplotlib import pyplot as plt

                            with np.errstate(divide="ignore", invalid="ignore"):
                                from cycler import cycler

                                plt.rc("axes", prop_cycle=cycler(color=["#f8961e", "#43aa8b"]))

                                mc_fakes_hist.plot_ratio(
                                    mc_closure_hist,
                                    rp_ylabel=r"Ratio",
                                    rp_denom_label="Fakes MC (scaled FR)",
                                    rp_num_label="Fakes MC (SR)",
                                    rp_uncert_draw_type="bar",
                                    rp_ylim=(0, 2),
                                )

                            fig = plt.gcf()
                            nax, rax = fig.axes

                            label(ax=nax, data=False, campaign=task.campaign_inst)

                            ratio = nom
                            ratio_up = fun(rebinned_x, nom, slope)
                            ratio_do = 2 * nom - fun(rebinned_x, nom, slope)

                            ncc_nom = rax.plot(
                                rebinned_x,
                                np.ones_like(rebinned_x) * ratio,
                                "-",
                                color="#a4161a",
                            )
                            ncc_shape = rax.fill_between(
                                rebinned_x,
                                ratio_up,
                                ratio_do,
                                facecolor="#a4161a",
                                alpha=0.2,
                            )

                            # fix ratio ylim
                            auto_ratio_ylim(rax, ratio_max_delta=1.2, auto_log_thresh=np.inf)
                            _handles, _labels = nax.get_legend_handles_labels()

                            # Add uncertainty to legend
                            _handles += [(ncc_nom[0], ncc_shape)]
                            _labels += ["Uncertainty"]

                            nax.legend_ = None
                            nax.legend(_handles, _labels)

                            title = task.analysis_inst.categories.get(
                                f"{flavour}_{cat}_sr_prompt{proc}"
                            ).label
                            fig.suptitle(title)

                            plt.xlabel(task.analysis_inst.variables.get(k).x_title)

                            # remove lowest label from upper plot
                            nax.yaxis.get_ticklabels()[0].set_visible(False)

                            plt.savefig(ncc_target.path)
                            fig.clf()

                        # get category of application
                        appl_cat = f"{flavour}_{cat}_sr_prompt{proc}"

                        # # save nominal
                        # nom = hv[axes.index("fakes", appl_cat, "nominal")].copy()

                        # # apply non-closure, scale nominal and all shapes
                        # hv[axes[:2].index("fakes", appl_cat)] *= fun(x, nom, slope)

                        # shifts
                        nom_down = lambda x: 1 - np.abs(1 - np.clip(nom, 0, 2))
                        nom_up = lambda x: 1 + np.abs(1 - np.clip(nom, 0, 2))
                        slope_down = lambda x: 1 - np.clip(slope * (x - cog), -1, 1)
                        slope_up = lambda x: 1 + np.clip(slope * (x - cog), -1, 1)

                        for shift, func in [
                            (("nom", flavour, "Down"), nom_down),
                            (("nom", flavour, "Up"), nom_up),
                            (("slope", flavour, "Down"), slope_down),
                            (("slope", flavour, "Up"), slope_up),
                        ]:
                            shift_name = fake_nc_shift_name(*shift)
                            hv[axes.index("fakes", appl_cat, shift_name)] *= func(x)
            return h

        groups1 = {
            "resolved": "resolved_[12]b",
            "incl": "boosted|resolved_[12]b",
            "top": "dnn_node_(st|tt)$",
            "otherhiggs": "dnn_node_(class_other|H)$",
            "tophiggs": "dnn_node_(st|tt|H)$",
            "wjets_other": "dnn_node_(wjets|class_other)$",
            # "type2_dy_vv": "dnn_node_(dy|class_multiboson)$",
            # "type2_other": "dnn_node_(st|tt|class_(rare|ttVX))$",
        }

        print("perform fakes extraction:", "fakes" in task.legacy_processes)
        for k, h in (tq := tqdm(hists.items(), unit="variable", desc="data driven modeling")):
            variable = task.analysis_inst.variables.get(k)
            groups2 = {"all": "^(e|mu)"}
            tq.set_postfix(step="grouping 1")
            h = cls.regroup(h, groups=groups1)
            if "shifts" in variable.tags:
                # Add TT bar dataset shifts
                # Must happen before fake extraction to propagate these shifts to the fakes process
                if "tt" in h.axes["process"] and cls.dataset_shifts:
                    h += topvariations(h)
            if "fakes" in task.legacy_processes:
                ncc_proc = "tt_fh"
                tq.set_postfix(step="fakes extraction")
                h = cls.data_mc_substract(
                    task.analysis_inst,
                    h,
                    fakes,
                    data_syst=data_syst,
                    skip_mc=fakes_skip_mc,
                )
                """
                application of fake non-closure shift:
                """
                if "systematic" in h.axes.name:
                    tq.set_postfix(step="prepare fake ncc")
                    if variable.tags.issuperset({"shifts", "dnn_score"}):
                        h = apply_ncc(h, ncc_proc=ncc_proc)
                    tq.set_postfix(step="apply fake ncc")

                # remove ncc_proc from h
                h = Hist5.regrow(h, dict(process=set(h.axes["process"]) - {ncc_proc}))

                groups2[None] = "(_fr|_nonprompt)"

            # add extra categories for variables which need standalone full syst. postfit plots
            if "has_category" in variable.tags:
                h = cls.regroup(h, groups={f"prompt_{variable.name}": "prompt"})

            tq.set_postfix(step="grouping 2")
            hists[k] = cls.regroup(h, groups=groups2)
        print(
            "Fake NCC Plots at",
            pscan_vispa(
                directory=os.path.join(task.output().parent.path, "fake_ncc"),
                regex=r"(?P<Lepton>[^\_]+)_(?P<Channel>.+)_sr_nonprompt_(?P<Category>.+)\.(?P<format>[^\.]+)",
            ),
        )
        return hists


class Exporter(common.Exporter, Base, MCOnly, ArrayExporter):
    debug_dataset = "GluGluToHHTo2B2WToLNu2J_node_SM"
    # debug_dataset = "TTToSemiLeptonic"

    @classmethod
    def requires(cls, task):
        return task.base_requires(data_source="mc")


class ExporterFlav(common.ExporterFlav, Exporter):
    debug_dataset = "GluGluToHHTo2B2WToLNu2J_node_SM"
    # debug_uuids = ["4ABE4FC8-EB78-1B4A-B956-D55FFF52D9AA"]
    # debug_dataset = "TTToSemiLeptonic"

    @property
    def tensors(self):
        out = super().tensors
        out["jet_flav"] = (
            "analysis_jets",
            6,
            ("flavour",),
            np.int32,
            {"groups": ["sort", "target"]},
        )
        return out


class BTagSFNormEff(Base, corr_proc.BTagSFNormEff):
    skip_cuts = {"==1bjets", ">=2bjets"}
    subjets = "good_fat_jets.subjets"
    debug_dataset = "GluGluToHHTo2B2WToLNu2J_node_SM"

    def select(self, *args, **kwargs):
        for ret in super().select(*args, **kwargs):
            cats = ret["categories"]
            for cat in list(cats.keys()):
                if cat.endswith("_fr"):
                    del cats[cat]
            yield ret


class SyncSelectionExporter(Base, MCOnly, TreeExporter):
    individual_weights = True
    skip_cats = set()

    local = "/net/scratch/cms/dihiggs/store/bbww_sl/Run2_pp_13TeV_2016/SyncSelection"
    sync_against = dict(
        tallinn=f"{local}/sync_bbww_Tallinn_2016_v19.root",
        louvain=f"{local}/sync_bbww_Louvain_2016_vG2.root",
    )

    tree_id = "syncTree_hhbb1l"

    # Sync with Gourab Saha 22.02.22
    debug_dataset = "TTToSemiLeptonic"
    debug_uuids = [
        "1A511872-6E69-6945-8499-B2696EC437AD",
        "9EBB05A3-E0F1-944C-929E-FED7F5A88926",
    ]  # year 2016

    groups = {
        "resolved": "resolved_[12]b",
        "incl": "boosted|resolved_[12]b",
        "_all_": "_(e|mu)_",
        None: "inclusive",
    }

    # fmt: off
    weights = ["PDFSet_off", "PDFSet_rel", "PSWeight_ISR", "PSWeight_FSR", "ScaleWeight_Fact", "ScaleWeight_Renorm", "ScaleWeight_Mixed", "ScaleWeight_Envelope", "gen_weight", "pileup", "l1_ecal_prefiring", "top_pT_reweighting", "trigger_muon_sf", "trigger_electron_sf", "btagWeight_subjet", "btagWeight", "btagWeight_lf", "btagWeight_lfstats1", "btagWeight_lfstats2", "btagWeight_hf", "btagWeight_hfstats1", "btagWeight_hfstats2", "btagWeight_cferr1", "btagWeight_cferr2", "btagNorm", "jet_PUid_efficiency", "jet_PUid_mistag", "fake_electrons", "fake_electrons_nom", "fake_electrons_pt", "fake_electrons_barrel", "fake_muons", "fake_muons_nom", "fake_muons_pt", "fake_muons_barrel", "fake_electrons_nc", "fake_electrons_nc_nom", "fake_electrons_nc_pt", "fake_electrons_nc_barrel", "fake_muons_nc", "fake_muons_nc_nom", "fake_muons_nc_pt", "fake_muons_nc_barrel"]
    # fmt: on

    def mask_channel(self, tree, channel=None):
        return np.s_[:]

    @property
    def tensors(self):
        common = ("energy", "x", "y", "z", "pt", "eta", "phi")
        tensors = super().tensors
        if not isinstance(tensors, dict):
            tensors = tensors.fget(self)
        # fmt: off
        tensors["electron"] = ("sync_electrons", 1, common + ("cone_pt", "pdgId", "charge", "mvaTTH"), np.float32, {"groups": ["part"]}) #  "gen_matched"
        tensors["muon"] = ("sync_muons", 1, common + ("cone_pt", "pdgId", "charge", "mvaTTH"), np.float32, {"groups": ["part"]}) # "gen_matched"
        tensors["presel_vbf_jets"] = ("presel_vbf_jets", 2, common, np.float32, {})
        if not ("data" in self.debug_dataset):
            tensors["weights"] = ("weights", 0, self.weights, np.float32, {})
        # fmt: on
        return tensors

    def arrays(self, X):
        out = super().arrays(X)
        for sel in X["selection"]._names:
            selection = X["selection"].require(**{sel: True})
            out[f"selection_{sel}"] = selection
        return out

    def categories(self, select_output):
        return {"all": slice(None)}

    def select(self, *args, **kwargs):
        for ret in super().select(*args, **kwargs):
            # remove GGF signal datasets
            ret["datasets"] = {None: 1}

            # add new selection bit for easier sync
            sel = ret["selection"]
            sel.add(
                "sync",
                sel.all(*ret["common"])
                & (sel.all(*ret["dim1"]["e"]) | sel.all(*ret["dim1"]["mu"]))
                & (
                    sel.all(*ret["dim2"]["resolved_1b"])
                    | sel.all(*ret["dim2"]["resolved_2b"])
                    | sel.all(*ret["dim2"]["boosted"])
                )
                & (sel.all(*ret["dim4"]["prompt"])),
            )
            yield ret


class SyncExporter(DNNBase, SyncSelectionExporter):
    output = "sync_01.root"

    @property
    def tensors(self):
        tensors = super().tensors
        if not isinstance(tensors, dict):
            tensors = tensors.fget(self)
        tensors["pred"] = (
            "pred",
            0,
            [
                "pred_GGF",
                "pred_VBF",
                "pred_TT",
                "pred_ST",
                "pred_WJets",
                "pred_H",
                "pred_Other",
            ],
            np.float32,
            {},
        )
        return tensors

    def select(self, *args, **kwargs):
        for ret in super().select(*args, **kwargs):
            for k in list(ret["categories"].keys()):
                if not "dnn_node_" in k:
                    ret["categories"].pop(k)
            yield ret


class FindBadGenJetMatch(BaseProcessor):
    debug_dataset = "TTTo2L2Nu"
    jes_shifts = False
    dataset_shifts = False

    def __init__(self, task):
        super().__init__(task)

        self._accumulator["bad"] = set_accumulator()

    @classmethod
    def requires(cls, task):
        return task.base_requires(data_source="mc", corrections=False, jetpuid={})

    def process(self, events):
        output = self.accumulator.identity()
        weights = processor.Weights(events.size)
        fix_GenJet(events)
        try:
            self.corrections["jetpuid"]["loose"](events.Jet, weights)
        except:
            output["bad"].add(self.get_lfn(events))
        return output


class TriggerPresel(BaseProcessor):
    debug_dataset = "GluGluToHHTo2B2VTo2L2Nu_node_cHHH1"
    trigger = Base.trigger

    @classmethod
    def requires(cls, task):
        return task.base_requires(corrections=False)

    def __init__(self, task):
        super().__init__(task)

        self._accumulator = dict_accumulator(
            n_events_before=defaultdict_accumulator(int),
            n_events_after=defaultdict_accumulator(int),
        )

    def process(self, events):
        output = self.accumulator.identity()
        dataset_key = tuple(events.metadata["dataset"])
        tr = []
        for k in self.trigger.values():
            tr.extend(list(k.keys()))
        m = util.nano_mask_or(events.HLT, tr, skipable=True)
        output["n_events_before"]["inclusive"] += len(events)
        output["n_events_before"][dataset_key] += len(events)
        output["n_events_after"]["inclusive"] += np.sum(m)
        output["n_events_after"][dataset_key] += np.sum(m)
        return output


class GetEvents(Processor):
    debug = True
    debug_dataset = "data_A_e+ee"
    debug_uuids = ["34A5D165-91CC-2840-A886-DC8F0A7B77D6"]

    def select(self, *args, **kwargs):

        for ret in super().select(*args, **kwargs):
            selection = ret["selection"]
            categories = ret["categories"]
            for cat, cuts in categories.items():
                mask = selection.all(*cuts)
                events = ret["events"][mask]
                if len(events.event) > 0:
                    print(cat, events.event[0])
            yield ret
