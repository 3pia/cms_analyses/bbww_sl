# coding: utf-8
# flake8: noqa
from math import pi
import utils.aci as aci


def setup_variables(cfg):
    # leptons
    for name, expr, title in [
        ("lep", "lepp4", "lepton"),
    ]:
        cfg.variables.add(
            aci.Variable(
                name=f"{name}_pt",
                expression=f"{expr}.pt",
                binning=(9, 30.0, 300.0),
                unit="GeV",
                x_title=rf"{title} $p_T$",
                tags={"has_category", "cat_check"},
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"{name}_eta",
                expression=f"{expr}.eta",
                binning=(25, -2.5, 2.5),
                x_title=rf"{title} $\eta$",
                tags={"has_category", "cat_check"},
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"{name}_phi",
                expression=f"{expr}.phi",
                binning=(32, -pi, pi),
                x_title=rf"{title} $\phi$",
            )
        )

    # jets
    cfg.variables.add(
        aci.Variable(
            name="njets",
            expression="ak.num(presel_jets)",
            binning=(8, 2.0, 10.0),
            x_title=r"Number of jets",
            tags={"has_category", "cat_check"},
        )
    )
    for i in range(4):
        # ak4 jets
        cfg.variables.add(
            aci.Variable(
                name=f"jet{i + 1}_pt",
                expression=f"util.normalize(analysis_jets.pt, pad=4, clip=True)[:, {i}]",
                binning=(17, 30.0, 200.0),
                unit="GeV",
                x_title=rf"Jet {i + 1} $p_T$",
                tags={"has_category", "cat_check"},
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"jet{i + 1}_eta",
                expression=f"util.normalize(analysis_jets.eta, pad=4, clip=True)[:, {i}]",
                binning=(25, -2.5, 2.5),
                x_title=rf"Jet {i + 1} $\eta$",
                tags={"has_category", "cat_check"},
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"jet{i + 1}_phi",
                expression=f"util.normalize(analysis_jets.phi, pad=4, clip=True)[:, {i}]",
                binning=(32, -pi, pi),
                x_title=rf"Jet {i + 1} $\phi$",
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"jet{i + 1}_btag",
                expression=f"util.normalize(analysis_jets.btagDeepFlavB, pad=4, clip=True)[:, {i}]",
                binning=(25, 0, 1),
                x_title=rf"Jet {i + 1} $Tag^B_{{DeepFlavour}}$",
                tags={"has_category", "cat_check"},
            )
        )

    # fat jets
    cfg.variables.add(
        aci.Variable(
            name="nfat_jets",
            expression="ak.num(good_fat_bjets)",
            binning=(10, 0.0, 10.0),
            x_title=r"Number of fat jets",
        )
    )
    for i in range(2):
        # ak4 jets
        cfg.variables.add(
            aci.Variable(
                name=f"fat_jet{i + 1}_pt",
                expression=f"util.normalize(good_fat_bjets.pt, pad=2, clip=True)[:, {i}]",
                binning=(52, 200.0, 1500.0),
                unit="GeV",
                x_title=rf"FatJet {i + 1} $p_T$",
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"fat_jet{i + 1}_eta",
                expression=f"util.normalize(good_fat_bjets.eta, pad=2, clip=True)[:, {i}]",
                binning=(25, -2.5, 2.5),
                x_title=rf"FatJet {i + 1} $\eta$",
            )
        )
        cfg.variables.add(
            aci.Variable(
                name=f"fat_jet{i + 1}_phi",
                expression=f"util.normalize(good_fat_bjets.phi, pad=2, clip=True)[:, {i}]",
                binning=(32, -pi, pi),
                x_title=rf"FatJet {i + 1} $\phi$",
            )
        )

    # met
    cfg.variables.add(
        aci.Variable(
            name="MET",
            expression="met.pt",
            binning=(40, 0.0, 200.0),
            unit="GeV",
            x_title=r"$p_{T}^{miss}$",
        )
    )
    cfg.variables.add(
        aci.Variable(
            name="MET_phi",
            expression="met.phi",
            binning=(32, -pi, pi),
            x_title=r"$\phi^{miss}$",
        )
    )

    # Event shape variables
    for postfix, posttitle in [("", ""), ("_met", " MET")]:
        for expr, title in [
            ("sphericity", "Sphericity"),
            ("sphericity_T", "Sphericity (transverse)"),
            ("aplanarity", "Aplanarity"),
            ("eventshape_C", "C"),
            ("eventshape_D", "D"),
            ("eventshape_Y", "Y"),
            ("centrality", "Centrality"),
            ("centrality_jets", "Centrality (jets)"),
        ]:
            cfg.variables.add(
                aci.Variable(
                    name=f"{expr}{postfix}",
                    expression=f"{expr}{postfix}",
                    binning=(50, 0, 1),
                    x_title=f"{title}{posttitle}",
                )
            )
        # eigenvalues
        for i in range(1, 4):
            cfg.variables.add(
                aci.Variable(
                    name=f"eigenvalue_{i}{postfix}",
                    expression=f"eigenvalue{i}{postfix}",
                    binning=(50, 0.0, 1.0),
                    x_title=f"Eigenvalue {i} of momentum tensor{posttitle}",
                )
            )
        for i in range(1, 6):
            if i == 1:
                binning = (50, 0.0, 1.0)
            else:
                binning = (100, -1.0, 1.0)
            cfg.variables.add(
                aci.Variable(
                    name=f"fox_wolfram_moment_{i}{postfix}",
                    expression=f"foxwolfram{i}{postfix}",
                    binning=binning,
                    x_title=f"Fox Wolfram moment {i}{posttitle}",
                )
            )

    # hl
    cfg.variables.add(
        aci.Variable(
            name="m_hh_bregcorr",
            expression="m_hh_bregcorr",
            binning=(23, 80.0, 1000.0),
            unit="GeV",
            x_title=r"HH $m^{reg}$",
            tags={"fit", "shifts", "has_category", "cat_hl"},
        )
    )
    cfg.variables.add(
        aci.Variable(
            name="m_hbb_bregcorr",
            expression="m_hbb_bregcorr",
            binning=(25, 0.0, 1000.0),
            unit="GeV",
            x_title=r"H(bb) $m^{reg}$",
            tags={"fit", "shifts", "has_category", "cat_hl"},
        )
    )

    for expr, title in [
        ("m_hww", r"H(WW) $m$"),
        ("m_wjj", r"W(jj) $m$"),
        ("m_wlep", r"$W_{lep}$ $m$"),
    ]:
        cfg.variables.add(
            aci.Variable(
                name=expr,
                expression=expr,
                binning=(50, 0.0, 1000.0),
                unit="GeV",
                x_title=title,
            )
        )

    for expr, title in [
        ("pt_hh", r"HH $p_T$"),
        ("pt_hww", r"H(WW) $p_T$"),
        ("pt_wjj", r"W(jj) $p_T$"),
        ("pt_wlep", r"$W_{lep}$ $p_T$"),
    ]:
        cfg.variables.add(
            aci.Variable(
                name=expr,
                expression=expr,
                binning=(60, 0.0, 600.0),
                unit="GeV",
                x_title=title,
            )
        )

    cfg.variables.add(
        aci.Variable(
            name="pt_hbb",
            expression="pt_hbb",
            binning=(20, 0.0, 600.0),
            unit="GeV",
            x_title=r"H(bb) $p_T$",
            tags={"fit", "shifts", "has_category", "cat_hl"},
        )
    )

    for expr, title in [
        ("dphi_hbb_hww", r"$\|\Delta\phi\|$ H(bb) H(WW)"),
        ("dphi_hbb_hwwvis", r"$\|\Delta\phi\|$ H(bb) H(WW)${}_{vis}$"),
        ("dphi_met_lep", r"$\|\Delta\phi\|$ Lep $p_{T}^{miss}$"),
        ("dphi_met_hbb", r"$\|\Delta\phi\|$ H(bb) $p_{T}^{miss}$"),
        ("dphi_met_wjj", r"$\|\Delta\phi\|$ W(jj) $p_{T}^{miss}$"),
        ("min_dhi_wjets", r"$\|\Delta\phi\|$ WJets"),
        ("min_dphi_bjets", r"$\|\Delta\phi\|$ bJets"),
    ]:
        cfg.variables.add(
            aci.Variable(
                name=expr,
                expression=f"ak.fill_none({expr}, np.nan)",
                binning=(32, 0, pi),
                x_title=title,
            )
        )

    for expr, title in [
        ("min_dr_lepbjets", r"min $\Delta$R Lep bJets"),
        ("dr_lep_hbb", r"$\Delta$R Lep H(bb)"),
        ("dr_lep_wjj", r"$\Delta$R Lep W(jj)"),
        ("min_dr_wjets", r"$\Delta$R WJets"),
        ("min_dr_bjets", r"$\Delta$R bJets"),
    ]:
        cfg.variables.add(
            aci.Variable(
                name=expr,
                expression=f"ak.fill_none({expr}, np.nan)",
                binning=(50, 0, 5),
                x_title=title,
            )
        )

    cfg.variables.add(
        aci.Variable(
            name="HT",
            expression="ht",
            binning=(19, 50.0, 1000.0),
            unit="GeV",
            x_title=r"$H_T$",
            tags={"fit", "shifts", "has_category", "cat_hl"},
        )
    )
    cfg.variables.add(
        aci.Variable(
            name="ht",
            expression="ht",
            binning=(19, 50.0, 1000.0),
            unit="GeV",
            x_title=r"$H_T$",
            aux={"histogram": False},
        )
    )

    cfg.variables.add(
        aci.Variable(
            name="smin",
            expression="smin",
            binning=(19, 50.0, 1000.0),
            unit="GeV",
            x_title=r"$S_{min}$",
            tags={"fit", "shifts", "has_category", "cat_hl"},
        )
    )

    cfg.variables.add(
        aci.Variable(
            name="vbf_pair_mass",
            expression="vbf_pair_mass",
            binning=(200, 0.0, 1000.0),
            unit="GeV",
            x_title=r"VBF Pair m",
        )
    )

    cfg.variables.add(
        aci.Variable(
            name=f"vbf_pairs_absdeltaeta",
            expression=f"vbf_pairs_absdeltaeta",
            binning=(25, 0, 5.0),
            x_title=r"VBF Pair $|\Delta\eta|$",
        )
    )

    cfg.variables.add(
        aci.Variable(
            name=f"lep_conept",
            expression=f"lep_conept",
            binning=(40, 0.0, 200.0),
            x_title=rf"Lep $p_T^{{cone}}$",
        )
    )

    for name, tag in [
        ("VBF", "vbf"),
        ("Boosted", "boosted"),
    ]:
        cfg.variables.add(
            aci.Variable(
                name=f"{tag}_tag",
                expression=f"{tag}_tag",
                binning=(2, 0, 2),
                x_title=rf"{name} Tag",
            )
        )

    cfg.variables.add(
        aci.Variable(
            name=f"n_btag",
            expression=f"n_btag",
            binning=(6, 0, 6),
            x_title=rf"N b-tag",
            tags={"has_category", "cat_check"},
        )
    )

    # file search variables
    for i in range(8):  # up to 32
        cfg.variables.add(
            aci.Variable(
                name=f"uuid{i}",
                expression=f"uuid[{i}]",
                binning=(16, 0, 16),
                x_title=f"UUID letter {i + 1}",
            )
        )

    cfg.variables.add(
        aci.Variable(
            name=f"jet1_btagDeepFlavB",
            expression=f"util.normalize(analysis_jets.btagDeepFlavB, pad=2, clip=True)[:, 1]",
            binning=(25, 0, 1),
            unit="1",
            x_title=rf"Jet 2 btagDeepFlavB",
            tags={"fit", "shifts", "has_category"},
        )
    )

    features = (
        ("energy", (60, 0, 300), "GeV", "$E$"),
        ("x", (60, 0, 300), "GeV", "$p_x$"),
        ("y", (60, 0, 300), "GeV", "$p_y$"),
        ("z", (60, 0, 300), "GeV", "$p_z$"),
        ("pt", (60, 0, 300), "GeV", "$p_T$"),
        ("cone_pt", (60, 0, 300), "GeV", "$p_T^{{cone}}$"),
        ("eta", (25, -2.5, 2.5), "1", "$\eta$"),
        ("phi", (32, -pi, pi), "1", "$\phi$"),
        ("pdgId", (20, 0, 20), "1", "PDGID"),
        ("charge", (3, -1, 1), "1", "Charge"),
        ("btagDeepFlavB", (25, 0, 1), "1", "btagDeepFlavB"),
        ("msoftdrop", (60, 0, 300), "GeV", "$m_{SD}$"),
    )
    for name, part_title in [
        ("lep", "Lepton"),
        ("electron", "Electron"),
        ("muon", "Muon"),
        ("jet", "Jet"),
        ("analysis_jets", "Jet"),
        ("presel_vbf_jets", "Presel VBF Jet"),
        ("fat", "AK8 Jet"),
    ]:
        for i in range(6):
            for feat, binning, unit, feat_title in features:
                key = f"{name}{i}_{feat}"
                if key in cfg.variables.keys:
                    continue
                cfg.variables.add(
                    aci.Variable(
                        name=key,
                        binning=binning,
                        unit=unit,
                        x_title=rf"{part_title} {i+1} {feat_title}",
                        aux={"histogram": False},
                    )
                )

    for name, part_title in [
        ("good_fat_bjets", "AK8 Jet"),
        ("lep_cone", "Lepton"),
    ]:
        features = list(features)
        for j in range(1, 5):
            features.append((f"tau{j}", (25, 0, 1), "1", rf"$\tau_{j}$"))
        for feat, binning, unit, feat_title in features:
            key = f"{name}_{feat}"
            if key in cfg.variables.keys:
                continue
            cfg.variables.add(
                aci.Variable(
                    name=key,
                    binning=binning,
                    unit=unit,
                    x_title=rf"{part_title} {feat_title}",
                    aux={"histogram": False},
                )
            )

    for name, title in [
        ("met", "MET"),
        ("metp4", "MET"),
    ]:
        for feat, binning in (
            ("energy", (60, 0, 300)),
            ("x", (60, 0, 300)),
            ("y", (60, 0, 300)),
            ("z", (60, 0, 300)),
            ("pt", (60, 0, 300)),
            ("eta", (25, -2.5, 2.5)),
            ("phi", (32, -pi, pi)),
        ):
            if f"{name}_{feat}" in cfg.variables.keys:
                continue
            cfg.variables.add(
                aci.Variable(
                    name=f"{name}_{feat}",
                    binning=binning,
                    unit="GeV",
                    x_title=rf"{title} {feat}",
                    aux={"histogram": False},
                )
            )

    # single histogram with max dnn prediction output
    cfg.variables.add(
        aci.Variable(
            name="dnn_score_max",
            expression="np.max(pred, axis=-1)",
            binning=(400, 0.0, 1.0),
            unit="",
            x_title=r"DNN score",
            tags={"fit", "shifts", "dnn_score"},
        )
    )

    for cf in ("cutflow", "cutflow_raw"):
        cfg.variables.add(
            aci.Variable(
                name=cf,
                aux={"histogram": False},
            )
        )
