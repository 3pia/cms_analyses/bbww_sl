# coding: utf-8
# flake8: noqa
import utils.aci as aci


def setup_categories(cfg):
    # dnn categories
    # use label for pretty printing
    # use label_short for latex
    get = cfg.processes.get

    cfg.aux["collate_cats"] = {
        ("_resolved_1b", "_resolved_2b", "_boosted"): ("_incl", "_incl3"),
        (r"_resolved(?!_\db)", "_boosted"): ("_incl", "_incl2"),
    }
    multiclass_config = cfg.aux["multiclass"]
    multiclass_processes = multiclass_config.groups[multiclass_config.group]
    classes = {
        f"dnn_node_{cls}": get(cls).label_short if "HH" in cls else get(cls).label
        for cls in multiclass_processes.keys()
    }
    classes.update(
        top="top",
        otherhiggs="otherhiggs",
        tophiggs="Top + Higgs",
        wjets_other="W+Jets + Other",
    )
    modes = {
        "incl": "inclusive",
        "inclusive": "inclusive (bad)",
        "boosted": "boosted",
        "resolved": r"resolved $\geq$ 1b",
        "resolved_1b": r"resolved = 1b",
        "resolved_2b": r"resolved $\geq$ 2b",
    }
    for ch, chtex in {"e": "e", "mu": r"μ", "all": "all"}.items():
        for mode, modetex in modes.items():
            nbase = f"{ch}_{mode}_sr_prompt"
            lbase = f"{chtex} {modetex}"
            cfg.categories.add(
                aci.Category(
                    name=nbase,
                    label=lbase,
                    aux={"fit_variable": "dnn_score_max"},
                )
            )
            for cls, ctex in classes.items():
                cfg.categories.add(
                    aci.Category(
                        name=f"{nbase}_{cls}",
                        label=f"{lbase}, {ctex}",
                        aux={"fit_variable": "dnn_score_max"},
                    )
                )

    for c in [
        "all_resolved_sr_prompt_tophiggs",
        "all_boosted_sr_prompt_tophiggs",
        "all_incl_sr_prompt_wjets_other",
    ] + [
        f"all_{region}_sr_prompt_dnn_node_{process}"
        for region in ["boosted", "resolved_1b", "resolved_2b"]
        for process in [p for p, g in multiclass_processes.items() if "fit" in g["groups"]]
    ]:
        cfg.categories.get(c).tags = {"fit"}
        if "HH" in c:
            cfg.categories.get(c).tags |= {"signal"}
            if "GluGlu" in c:
                cfg.categories.get(c).tags |= {"ggF"}
            if "VBF" in c:
                cfg.categories.get(c).tags |= {"VBF"}
            if "boosted" in c:
                cfg.categories.get(c).tags |= {"boosted"}
            if "resolved_1b" in c:
                cfg.categories.get(c).tags |= {"resolved_1b"}
            if "resolved_2b" in c:
                cfg.categories.get(c).tags |= {"resolved_2b"}
        else:
            cfg.categories.get(c).tags |= {"background"}

    # categories for additional plots of variables
    for var in cfg.variables:
        if "has_category" in var.tags:
            cat_tags = {tag[4:] for tag in var.tags if tag.startswith("cat_")}
            cfg.categories.add(
                aci.Category(
                    name=f"all_incl_sr_prompt_{var.name}",
                    label=f"",
                    aux={"fit_variable": var.name},
                    tags=cat_tags,
                )
            )
