# coding: utf-8
# flake8: noqa

"""
Definition of the diHiggs analysis in the bbWW (SL) channel.
"""

import re

#
# analysis and config
#
from tasks.corrections.gf_hh import HHReweigthing
from config.util import PrepareConfig
from utils.aci import rgb
from config.constants import (
    BR_HH_BBTAUTAU,
    BR_HH_BBWW_SL,
    BR_HH_BBVV_DL,
    HHres,
    HHxs,
    gfHHparams,
    gfHHparamsNoSample,
)
from tasks.multiclass import MulticlassConfig
import utils.aci as aci
from config.analysis import analysis


# create the analysis
analysis = analysis.copy(name="diHiggs_sl", label="Single-lepton", label_short="bbWW (SL)")
get = analysis.processes.get

# add analysis specific processes
specific_processes = []

# fmt: off
dc_others = aci.Process(name="dc_others", id=999989, label=r"Others", color=rgb(103, 179, 153), processes=[get("rare")])
specific_processes.append(dc_others)

dc_ttVX = aci.Process(name="dc_ttVX", id=999990, label="ttVX", processes=[get("ttV"), get("ttVV")])
specific_processes.append(dc_ttVX)

dc_ggHbb = aci.Process(name="dc_ggHbb", id=23412341, label="ggH(bb)", processes=[get("GluGluHToBB_M125"), get("GluGluHToTauTau_M125")])
specific_processes.append(dc_ggHbb)

dc_qqHbb = aci.Process(name="dc_qqHbb", id=23412351, label="qqH(bb)", processes=[get("VBFHToBB_M_125"), get("VBFHToTauTau_M125")])
specific_processes.append(dc_qqHbb)

dc_ZHbb = aci.Process(name="dc_ZHbb", id=23412361, label="ZH(bb)", processes=[get("ZH_DY"), get("ggZH"), get("ZHToTauTau_M125")])
specific_processes.append(dc_ZHbb)

plot_vv_v = aci.Process(name="plot_vv(v)", id=999991, color=rgb(82, 124, 182), label=r"Multiboson", processes=[get("vv"), get("vvv")])
specific_processes.append(plot_vv_v)

plot_other = aci.Process(name="plot_other", id=999992, color=rgb(152, 78, 163), label=r"Other", processes=[get("rare"), get("ttV"), get("ttVV")])
specific_processes.append(plot_other)

class_multiboson = aci.Process(name="class_multiboson", id=3789273, label="VV(V)", processes=[get("vv"), get("vvv")])
specific_processes.append(class_multiboson)

class_ttVX = aci.Process(name="class_ttVX", id=3789274, label="ttVX", processes=[get("ttV"), get("ttVH"), get("ttVV")])
specific_processes.append(class_ttVX)

# content is generated on-the-fly, don't whine about no datasets [dynamic]
fakes = aci.Process(name="fakes", id=999993, color=rgb(137, 207, 240), label="Fakes", is_data=True, aux=dict(dynamic=True))
specific_processes.append(fakes)

class_HHGluGlu_NLO = aci.Process(name="class_HHGluGlu_NLO", id=17789277, label="HH(ggF)", processes=[get("HH_2B2WToLNu2J_GluGlu_NLO"), get("HH_2B2VTo2L2Nu_GluGlu_NLO"), get("HH_2B2Tau_GluGlu_NLO")])
specific_processes.append(class_HHGluGlu_NLO)
    
class_HHVBF_NLO = aci.Process(name="class_HHVBF_NLO", id=17789278, label="HH(VBF)", processes=[get("HH_2B2WToLNu2J_VBF_NLO"), get("HH_2B2VTo2L2Nu_VBF_NLO"), get("HH_2B2Tau_VBF_NLO")])
specific_processes.append(class_HHVBF_NLO)

class_other = aci.Process(name="class_other", id=3789275, label="Other", processes=[get("dy"), get("ttV"), get("ttVV"), get("vv"), get("vvv"), get("rare")])
specific_processes.append(class_other)

plot_H = aci.Process(name="plot_H", id=23412371, label=r"(SM) Higgs", color=rgb(120, 33, 80), processes=[get("ttVH"), get("HZJ_HToWW_M125"), get("TH"), get("ttH"), get("WH"), dc_ggHbb, dc_qqHbb, dc_ZHbb, get("GluGluHToWWToLNuQQ_M125_NNPDF31"), get("VBFHToWWToLNuQQ_M125_NNPDF31")])
specific_processes.append(plot_H)

plot_others = aci.Process(name="plot_others", id=23412372, color=rgb(152, 78, 163), label=r"Others", processes=[dc_others, get("ttV"), get("ttVV")])
specific_processes.append(plot_others)

# fmt: on

# merge signals
specific_signals = []
signals = (
    "qqHH_CV_1_C2V_1_kl_0",
    "qqHH_CV_1_C2V_1_kl_1",
    "qqHH_CV_1_C2V_1_kl_2",
    "qqHH_CV_1_C2V_2_kl_1",
    "qqHH_CV_1_5_C2V_1_kl_1",
    "qqHH_CV_0_5_C2V_1_kl_1",
    "qqHH_CV_1_C2V_0_kl_1",
    "ggHH_kl_0_kt_1",
    "ggHH_kl_1_kt_1",
    "ggHH_kl_2p45_kt_1",
    "ggHH_kl_5_kt_1",
)
for i, s in enumerate(signals):
    sl = get(f"{s}_2B2WToLNu2J")
    dl = get(f"{s}_2B2VTo2L2Nu")
    ls = dl.label_short.replace(
        r"HH$, $H \rightarrow b\bar{b}$, $H \rightarrow VV$ (DL)",
        r"HH \rightarrow b\bar{b}VV$" + "\n",
    ).replace("\n ", "\n")
    if s in ("qqHH_CV_1_C2V_1_kl_1", "ggHH_kl_1_kt_1"):
        ls = re.sub(r"\s*\(NLO\) -.+", " (NLO, SM)", ls)
    p = aci.Process(
        name=f"{s}_2B2V",
        id=1900 + i,
        label=dl.label.replace(" (DL)", ""),
        label_short=ls,
        processes=[sl, dl],
    )
    # add to parent
    get("diHiggs").processes.add(p)
    specific_signals.append(p)

# add all processes now
analysis.processes.extend(specific_processes + specific_signals)

# corrections which are only used for distinct analyses
analysis.aux["non_common_corrections"] = ["Fake", "BTagSubjetCorrections"]
analysis.aux["doFakeNonClosureCorrection"] = True

# categories and files used for the sync of yields
# fmt: off
analysis.aux["sync_yield"] = {
    "category": "(all_(boosted|resolved_1b|resolved_2b)_sr_prompt_dnn_node_class_HH.+_NLO|all_(boosted|resolved_1b|resolved_2b)_sr_prompt_dnn_node_H|all_(boosted|resolved)_sr_prompt_dnn_node_tt|all_(boosted|resolved)_sr_prompt_dnn_node_wjets|all_incl_sr_prompt$)",
    "paths": {"TLL": "/net/scratch/cms/dihiggs/sync/tll_sl_2016.json"},
    "lut": {
        "all_incl_sr_prompt": "total",
        "all_resolved_1b_sr_prompt_dnn_node_H": "H_resolved_1b",
        "all_resolved_2b_sr_prompt_dnn_node_H": "H_resolved_2b",
        "all_boosted_sr_prompt_dnn_node_H": "H_boosted",
        "all_resolved_sr_prompt_dnn_node_tt": "TT_resolved",
        "all_boosted_sr_prompt_dnn_node_tt": "TT_boosted",
        "all_resolved_sr_prompt_dnn_node_wjets": "W_resolved",
        "all_boosted_sr_prompt_dnn_node_wjets": "W_boosted",
    }
}
# fmt: on
#
# process groups
#

# shorten label for plotting:
# fmt: off
# analysis.processes.get("ggHH_kl_1_kt_1_2B2V").label_short = "$gg \\rightarrow HH \\rightarrow b\\bar{b}VV$ (SM)"
# analysis.processes.get("qqHH_CV_1_C2V_1_kl_1_2B2V").label_short = "$VBF \\rightarrow HH \\rightarrow b\\bar{b}VV$ (SM)"
# shorten it like in bbtautau
analysis.processes.get("ggHH_kl_1_kt_1_2B2V").label_short = "HH (ggF)"
analysis.processes.get("qqHH_CV_1_C2V_1_kl_1_2B2V").label_short = "HH (VBF)"
# fmt: on


def pgrp(name: str, children: tuple, **kwargs) -> str:
    ret = analysis.processes.new(name=name, processes=list(map(get, children)), **kwargs)
    sig = get("diHiggs")
    if any(map(sig.has_process, ret.processes)):
        sig.processes.add(ret)
    return ret.name


processes = [
    "tt",
    "dy",
    "st",
    "fakes",
    "wjets",
    "plot_vv(v)",
    "plot_others",
    "plot_H",
    "qqHH_CV_1_C2V_1_kl_1_2B2V",
    "ggHH_kl_1_kt_1_2B2V",
    "data",
]
analysis.aux["process_groups"] = {
    # just defines the grouping during plotting/inference
    "plotting": processes,
    "plotting_fake_nc": processes + ["tt_fh"],
    "compact": [
        # fmt: off
        pgrp("compact_top", ["tt", "st"], color=rgb(193, 117, 166), label="Top"),
        pgrp("compact_wjets", ["wjets"], color=rgb(161, 193, 129), label="W+Jets"),
        pgrp("compact_fake", ["fakes"], color=rgb(168, 218, 220), label="Misid. lep."),
        pgrp("compact_other", ["dy", "plot_vv(v)","plot_others", "plot_H"], color=rgb(249, 216, 117), label="Other"),
        pgrp("ggHH_compact", ["ggHH_kl_1_kt_1_2B2V", "ggHH_kl_1_kt_1_2B2Tau"], color=rgb(200, 37, 6), label="HH (ggF)"),
        pgrp("qqHH_compact", ["qqHH_CV_1_C2V_1_kl_1_2B2V", "qqHH_CV_1_C2V_1_kl_1_2B2Tau"], color=rgb(6, 37, 200), label="HH (VBF)"),
        # fmt: on
        "data",
    ],
    "dev": ["data", "tt", "tt_fh", "ggHH_kl_1_kt_1_2B2V", "qqHH_CV_1_C2V_1_kl_1_2B2V", "fakes"],
}

analysis.aux["btag_sf_shifts"] = [
    "lf",
    "lfstats1",
    "lfstats2",
    "hf",
    "hfstats1",
    "hfstats2",
    "cferr1",
    "cferr2",
]

analysis.aux["sync"] = {
    "lookup_set": {
        (b">=", b"geq"),
        (b"==", b"eq"),
        (b"<=", b"leq"),
        (b">", b"g"),
        (b"<", b"l"),
    },
    "categories": ["is_all_incl_sr_prompt"],
    "eventnr": "eventnr",
    "int_vars": ["eventnr", "dataset_id"],
}
PrepareConfig(
    analysis,
    # defines which datasets we will use
    processes=[
        "data",
        "HH_2B2WToLNu2J",
        "HH_2B2VTo2L2Nu",
        "HH_2B2Tau",
        "H",
        "tt",
        # "dy_lep_10To50",  # remove for new prod
        # "dy_lep_50ToInf",  # remove for new prod
        "dy_lep_nj",
        "vv",
        "st",
        "WJetsToLNu",
        "WJetsToLNu_nJ",
        # "WnJetsToLNu",
        # "WJetsToLNu_HT",
        # "WBJetsToLNu_Wpt",
        # "WJetsToLNu_BGenFilter_Wpt",
        "ttV",
        "ttVV",
        "ttVH",
        "vvv",
        "rare",
        # "QCD",  # -> already included in fakes process
    ],
    skip_process=lambda process: process.name.endswith(HHres["resonance"]),
    ignore_datasets=[
        # "TTToHadronic",
        # "THQ_ctcvcp_4f_Hincl",
    ],  # , "ZH_HToBB_ZToLL_M125", "TTWW"],
    allowed_exclusive_processes=[
        "wjets_lep",
        # "dy_50to",
    ],
)
# HACK: remove unwanted inclusive WJets datasets (WJetsToLNu) where NLO nJ-binned is available
for y in 2017, 2018:
    d = analysis.campaigns[f"Run2_pp_13TeV_{y}"].datasets
    d.remove(d.get("WJetsToLNu"))

HHReweigthing.update_config(
    config=analysis,
    root_processes={
        "HH_2B2WToLNu2J_GluGlu": BR_HH_BBWW_SL,
        "HH_2B2VTo2L2Nu_GluGlu": BR_HH_BBVV_DL,
        "HH_2B2Tau_GluGlu": BR_HH_BBTAUTAU,
    },
    benchmarks=(
        [dict(kl=x) for x in HHxs["gf_nnlo_ftapprox"].keys()]
        + [
            dict(
                name=f"BM12_{i}",
                **{k: gfHHparams[k][i] for k in gfHHparams.dtype.names},
            )
            for i in range(1, len(gfHHparams))  # skip first, since SM := kl=1
        ]
        + [
            dict(
                name="BM12_8a" if i == 0 else f"BM7_{i}",
                **{k: gfHHparamsNoSample[k][i] for k in gfHHparamsNoSample.dtype.names},
            )
            for i in range(len(gfHHparamsNoSample))
        ]
    ),
)
class_HHGluGlu_NLO_reweight = aci.Process(
    name="class_HHGluGlu_NLO_reweight",
    id=17789287,
    label="HH(ggF)",
    processes=[
        proc
        for parent_process in [
            "HH_2B2WToLNu2J_GluGlu_reweight",
            "HH_2B2VTo2L2Nu_GluGlu_reweight",
            "HH_2B2Tau_GluGlu_reweight",
        ]
        for proc in analysis.processes.get(parent_process).processes.query(".*0_0_0")
    ],
)
analysis.processes.extend([class_HHGluGlu_NLO_reweight])

analysis.aux["multiclass"] = MulticlassConfig(
    groups={
        "default": {
            "class_HHGluGlu_NLO": {"groups": ["signal", "fit", "norm"]},
            "class_HHVBF_NLO": {"groups": ["signal", "fit", "norm"]},
            "tt": {"groups": ["background"]},
            "st": {"groups": ["background"]},
            "wjets": {"groups": ["background"]},
            "H": {"groups": ["background"]},
            "class_other": {"groups": ["background"]},
        },
        "reweight": {
            "class_HHGluGlu_NLO_reweight": {"groups": ["signal", "fit", "norm"]},
            "class_HHVBF_NLO": {"groups": ["signal", "fit", "norm"]},
            "tt": {"groups": ["background"]},
            "st": {"groups": ["background"]},
            "wjets": {"groups": ["background"]},
            "H": {"groups": ["background"]},
            "class_other": {"groups": ["background"]},
        },
        "ggfttbar": {
            "ggHH_kl_1_kt_1_2B2WToLNu2J": {"groups": ["signal", "fit"]},
            "tt": {"groups": ["background"]},
        },
    },
    group="default",
    maxn=int(2e6),
)

from bbww_sl.config.variables import setup_variables

setup_variables(analysis)

from bbww_sl.config.categories import setup_categories

setup_categories(analysis)

#
# other configurations
#

analysis.aux["signal_process"] = "diHiggs"


analysis.aux["binning_categories"] = {
    "2016": [
        "*_boosted_?r_*rompt_dnn_node_H",
        "*_boosted_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_boosted_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_boosted_?r_*rompt_dnn_node_class_other",
        "*_boosted_?r_*rompt_dnn_node_st",
        "*_boosted_?r_*rompt_dnn_node_tt",
        "*_boosted_?r_*rompt_dnn_node_wjets",
        "*_boosted_?r_*rompt_otherhiggs",
        "*_boosted_?r_*rompt_top",
        "*_boosted_?r_*rompt_tophiggs",
        "*_boosted_?r_*rompt_wjets_other",
        "*_incl_?r_*rompt_dnn_node_class_other",
        "*_incl_?r_*rompt_dnn_node_st",
        "*_incl_?r_*rompt_dnn_node_tt",
        "*_incl_?r_*rompt_dnn_node_wjets",
        "*_incl_?r_*rompt_otherhiggs",
        "*_incl_?r_*rompt_top",
        "*_incl_?r_*rompt_tophiggs",
        "*_incl_?r_*rompt_wjets_other",
        "*_resolved_1b_?r_*rompt_dnn_node_H",
        "*_resolved_1b_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_1b_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_resolved_2b_?r_*rompt_dnn_node_H",
        "*_resolved_2b_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_2b_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_resolved_?r_*rompt_dnn_node_H",
        "*_resolved_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_resolved_?r_*rompt_dnn_node_class_other",
        "*_resolved_?r_*rompt_dnn_node_st",
        "*_resolved_?r_*rompt_dnn_node_tt",
        "*_resolved_?r_*rompt_dnn_node_wjets",
        "*_resolved_?r_*rompt_otherhiggs",
        "*_resolved_?r_*rompt_top",
        "*_resolved_?r_*rompt_tophiggs",
        "*_resolved_?r_*rompt_wjets_other",
    ],
    "2017": [
        "*_boosted_?r_*rompt_dnn_node_H",
        "*_boosted_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_boosted_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_boosted_?r_*rompt_dnn_node_class_other",
        "*_boosted_?r_*rompt_dnn_node_st",
        "*_boosted_?r_*rompt_dnn_node_tt",
        "*_boosted_?r_*rompt_dnn_node_wjets",
        "*_boosted_?r_*rompt_otherhiggs",
        "*_boosted_?r_*rompt_top",
        "*_boosted_?r_*rompt_tophiggs",
        "*_boosted_?r_*rompt_wjets_other",
        "*_incl_?r_*rompt_dnn_node_class_other",
        "*_incl_?r_*rompt_dnn_node_st",
        "*_incl_?r_*rompt_dnn_node_tt",
        "*_incl_?r_*rompt_dnn_node_wjets",
        "*_incl_?r_*rompt_otherhiggs",
        "*_incl_?r_*rompt_top",
        "*_incl_?r_*rompt_tophiggs",
        "*_incl_?r_*rompt_wjets_other",
        "*_resolved_1b_?r_*rompt_dnn_node_H",
        "*_resolved_1b_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_1b_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_resolved_2b_?r_*rompt_dnn_node_H",
        "*_resolved_2b_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_2b_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_resolved_?r_*rompt_dnn_node_H",
        "*_resolved_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_resolved_?r_*rompt_dnn_node_class_other",
        "*_resolved_?r_*rompt_dnn_node_st",
        "*_resolved_?r_*rompt_dnn_node_tt",
        "*_resolved_?r_*rompt_dnn_node_wjets",
        "*_resolved_?r_*rompt_otherhiggs",
        "*_resolved_?r_*rompt_top",
        "*_resolved_?r_*rompt_tophiggs",
        "*_resolved_?r_*rompt_wjets_other",
    ],
    "2018": [
        "*_boosted_?r_*rompt_dnn_node_H",
        "*_boosted_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_boosted_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_boosted_?r_*rompt_dnn_node_class_other",
        "*_boosted_?r_*rompt_dnn_node_st",
        "*_boosted_?r_*rompt_dnn_node_tt",
        "*_boosted_?r_*rompt_dnn_node_wjets",
        "*_boosted_?r_*rompt_otherhiggs",
        "*_boosted_?r_*rompt_top",
        "*_boosted_?r_*rompt_tophiggs",
        "*_boosted_?r_*rompt_wjets_other",
        "*_incl_?r_*rompt_dnn_node_class_other",
        "*_incl_?r_*rompt_dnn_node_st",
        "*_incl_?r_*rompt_dnn_node_tt",
        "*_incl_?r_*rompt_dnn_node_wjets",
        "*_incl_?r_*rompt_otherhiggs",
        "*_incl_?r_*rompt_top",
        "*_incl_?r_*rompt_tophiggs",
        "*_incl_?r_*rompt_wjets_other",
        "*_resolved_1b_?r_*rompt_dnn_node_H",
        "*_resolved_1b_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_1b_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_resolved_2b_?r_*rompt_dnn_node_H",
        "*_resolved_2b_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_2b_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_resolved_?r_*rompt_dnn_node_H",
        "*_resolved_?r_*rompt_dnn_node_class_HHGluGlu_NLO",
        "*_resolved_?r_*rompt_dnn_node_class_HHVBF_NLO",
        "*_resolved_?r_*rompt_dnn_node_class_other",
        "*_resolved_?r_*rompt_dnn_node_st",
        "*_resolved_?r_*rompt_dnn_node_tt",
        "*_resolved_?r_*rompt_dnn_node_wjets",
        "*_resolved_?r_*rompt_otherhiggs",
        "*_resolved_?r_*rompt_top",
        "*_resolved_?r_*rompt_tophiggs",
        "*_resolved_?r_*rompt_wjets_other",
    ],
}
